function mytest()
    print("mytest")
end


-- helper function to print nicely formatted message
function os_message(text)
  local mymax = 77 - string.len(text) - string.len("done")
  local msg = text.." "..string.rep(".",mymax).." done"
  return print(msg)
end

-- helper function to run a command and capture its output
function os_capture(cmd, raw)
  local f = assert(io.popen(cmd, 'r'))
  local s = assert(f:read('*a'))
  f:close()
  if raw then return s end
    s = string.gsub(s, '^%s+', '')
    s = string.gsub(s, '%s+$', '')
    s = string.gsub(s, '[\n\r]+', ' ')
  return s
end



function docinit_hook()
  local sourcefiledir = maindir.."/src/latexmk/flat"
  errorlevel = cp("latexmkrc", sourcefiledir, typesetdir)
  if errorlevel ~= 0 then
    error("** Error!!: Can't copy latexmkrc from "..sourcefiledir.." to "..typesetdir)
    return errorlevel
  else
    print("** Copying latexmkrc from "..sourcefiledir.." to "..typesetdir)
  end
  return 0
end


-- typeset helper function: use this function to customize typesetting
function typeset(filename)

    -- skip compilation, if no base was given
    if not base then
        return 0
    end

    --  try to build the documentation file
    -- TODO: unfortunately, I didn't find a way to completely suppress the latexmk output here
    errorlevel = run(typesetdir, "TEXINPUTS=../../"..base.."/distrib/tds//: latexmk -quiet -pdf "..filename.." >"..os_null)
    -- print("errorlevel", errorlevel)
    -- latexmk can finish with errorlevel 0: everything is good
    if errorlevel ~= 0 then
        error("** Error!!: could not compile '"..filename.."'. Check respective log for errors.")
    end

    return 0
end