require 'lfs'
require 'auxiliary_functions'
require "version"

maindir = "."
builddir = maindir.."/build"

sourcefiledir = maindir

flatname = "flat"
structuredname = "structured"
buildname = "ic-templates"


-- set the list of modules used by build target ctan
-- since I decided to use the config/subdir directory structure, module names need to be prefixed by "config/"
-- for each module, the build process is carried out according to config/modulename/build.lua
-- make sure, that dependencies are met by respecting the order of modules here:
modules = {
    "config/ic-book",
    "config/ic-thesis",
    "config/ic-slides",
    "config/ic-exercise",
    "config/ic-thesisnotice",
    "config/ic-book-template",
    "config/ic-book-demo1",
    "config/ic-book-demo2",
    "config/ic-thesis-template",
    "config/ic-thesis-demo1",
    "config/ic-slides-template",
    "config/ic-slides-demo1",
    "config/ic-slides-demo2",
    "config/ic-exercise-template",
    "config/ic-exercise-demo1",
    "config/ic-thesisnotice-template",
    "config/ic-thesisnotice-demo1"
}



-- manually specify which files to process for tagging (relative to sourcefiledir)
tagfiles = {
    "src/**/*.cls",
    "src/**/*.clo",
    "src/**/*.sty",
    "src/**/*.tex",
    "src/**/README.md",
    -- "example.multiple.points",
    -- "filewithoutsuffix",
    -- "latexmkrc",
}

-- store current date
local mydate = os.date("!%Y-%m-%d")

-- replacement patern for expl classes
local r_clsheader_expl = "\\ProvidesExplClass%s-{(.-)}%s-{(.-)}%s-{(.-)}%s-{(.-)}";


-- replacement pattern for styles
local r_styheader_expl = "\\ProvidesExplPackage%s-{(.-)}%s-{(.-)}%s-{(.-)}%s-{(.-)}";


-- replacement patterns for versioning comments in tex-type files, e.g. .sty, .cls, .clo, .tex, .tikz
local r_texcomment = "%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n%% %%%%\n(.+)\n%% %%%%\n%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%";


-- replacement patterns for markdown files, e.g. README.md
local r_readme = "The current version of bundle ``(.-)`` is ``(v.-)``, released on ``(.-)``%."



-- TODO: remove?
-- define build targets
modules_to_build = {
    -- "ic-test",
    -- "ic-book",
    -- "ic-book-template",
    -- "ic-book-demo1",
    -- "ic-book-demo2",
    -- "ic-thesis",
    -- "ic-thesis-template",
    -- "ic-thesis-demo1",
    -- "ic-slides",
    -- "ic-slides-template",
    -- "ic-slides-demo1",
    -- "ic-slides-demo2",
    -- "ic-exercise",
    -- "ic-exercise-template",
    -- "ic-exercise-demo1",
    -- "ic-thesisnotice",
    -- "ic-thesisnotice-template",
    -- "ic-thesisnotice-demo1",
    -- "ic-exam"
}



-- clean up the builddir and notify the user in case of success or failure
function clean_builddir()
  if direxists(builddir) then
    errorlevel = cleandir(builddir)
    if not errorlevel  then
      error("** Error!!: The directory '"..builddir.."' could not be deleted")
    end
    os_message("** Cleaning up '"..builddir.."' to start from a clean slate")
  end
end





-- this function is responsible for actually implementing the tagging mechanism of l3build. When l3build is called with target 'tag', it will loop over the files found according to variable tagfiles, and call update_tag(...) for each with the following arguments:
-- file: the filename of the current file being tagged (without base path, but with suffix)
-- content: full content of file being tagged
-- tagname: if called by 'l3build tag tagname', else nil
-- tagdate: if specified manually by calling 'l3build tag --date YYYY-MM-DD', else current day
function update_tag(file, content, tagname, tagdate)
  -- check arguments and use sensible defaults
  if not tagname and tagdate == mydate then
    tagname = pkgversion
    tagdate = pkgdate
    print("** "..file.." has been tagged with the version 'v"..tagname.."' and date '"..tagdate.."' from version.lua")
  else
    local v_maj, v_min, v_hotfix = string.match(tagname, "^v?(%d+)%.(%d+)%.(%d+%S*)$")
    if v_maj == "" or not v_min or not v_hotfix then
      print("!! Error: Invalid tag '"..tagname.."', none of the files have been tagged")
      os.exit(0)
    else
      tagname = string.format("%i.%i.%s", v_maj, v_min, v_hotfix)
      tagdate = mydate
    end
    print("** "..file.." has been tagged with the version 'v"..tagname.."' and date "..mydate)
  end

  local p_clsheader_expl = "\\ProvidesExplClass {%1} {"..string.gsub(tagdate, "-", "/").."} {"..tagname.."} {%4}"
  local p_styheader_expl = "\\ProvidesExplPackage {%1} {"..string.gsub(tagdate, "-", "/").."} {"..tagname.."} {%4}"
  local p_texcomment = "%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n%% %%%%\n%% %%%% "..affiliation.."\n%% %%%% For additional information visit "..repourl.."\n%% %%%% Release Version  : "..tagname.."\n%% %%%% Release Date     : "..tagdate.."\n%% %%%%\n%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
  local p_readme = "The current version of bundle ``%1`` is ``v"..tagname.."``, released on ``"..string.gsub(tagdate, "(%d+)%-(%d+)%-(%d+)", "%3.%2.%1").."``."


  -- replace class header, if available
  content = string.gsub( content, r_clsheader_expl, p_clsheader_expl )
  content = string.gsub( content, r_styheader_expl, p_styheader_expl )
  content = string.gsub( content, r_texcomment, p_texcomment)
  content = string.gsub( content, r_readme, p_readme)

  return content
end


-- helper function to print tables. This is not necessary for the build process, but has been implemented for more convenient debugging.
function print_table(node)
    local cache, stack, output = {},{},{}
    local depth = 1
    local output_str = "{\n"

    while true do
        local size = 0
        for k,v in pairs(node) do
            size = size + 1
        end

        local cur_index = 1
        for k,v in pairs(node) do
            if (cache[node] == nil) or (cur_index >= cache[node]) then

                if (string.find(output_str,"}",output_str:len())) then
                    output_str = output_str .. ",\n"
                elseif not (string.find(output_str,"\n",output_str:len())) then
                    output_str = output_str .. "\n"
                end

                -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
                table.insert(output,output_str)
                output_str = ""

                local key
                if (type(k) == "number" or type(k) == "boolean") then
                    key = "["..tostring(k).."]"
                else
                    key = "['"..tostring(k).."']"
                end

                if (type(v) == "number" or type(v) == "boolean") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = "..tostring(v)
                elseif (type(v) == "table") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = {\n"
                    table.insert(stack,node)
                    table.insert(stack,v)
                    cache[node] = cur_index+1
                    break
                else
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = '"..tostring(v).."'"
                end

                if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                else
                    output_str = output_str .. ","
                end
            else
                -- close the table
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                end
            end

            cur_index = cur_index + 1
        end

        if (size == 0) then
            output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
        end

        if (#stack > 0) then
            node = stack[#stack]
            stack[#stack] = nil
            depth = cache[node] == nil and depth + 1 or depth - 1
        else
            break
        end
    end

    -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
    table.insert(output,output_str)
    output_str = table.concat(output)

    print(output_str)
end


--   now check_marked_tags(): this just reads in some source files, then regex matches a pattern, and finds the tags in file. If matched, prints a status updte, if not matches, asks the user to run l3build tag
function check_marked_tags()
    -- build a table of files, which need to be checked:
    --   these are the same files used for tagging
    local files_to_check = {}
    for _,filepattern in ipairs(tagfiles) do
      local a = tree(sourcefiledir, filepattern)
      for _,file in ipairs(a) do
        table.insert(files_to_check, file)
      end
    end
    -- debug: print files to check / tag
    -- print("files_to_check:")
    -- print_table(files_to_check)

    for _,file in ipairs(files_to_check) do
      local f = assert(io.open(file['src'], "r"))
      filecontents = f:read("*all")
      f:close()

      file['name'] = string.gsub(file['src'], ".+/(.+)", "%1")

      -- checking .cls files
      local m_clsname, m_clsdate, m_clsversion, m_clsdesc = string.match( filecontents, r_clsheader_expl)
      if m_clsname ~= nil and m_clsdate == string.gsub(pkgdate, "-", "/") and m_clsversion == pkgversion then
        os_message("** Checking version and date in "..file['name']..": OK")
      end
      if (m_clsname~=nil) and ( m_clsdate ~= string.gsub(pkgdate, "-", "/") or m_clsversion ~= pkgversion ) then
        print("** Warning: "..file['name'].." is marked with version "..m_clsversion.." and date "..m_clsdate)
        print("** Warning: version.lua is marked with version "..pkgversion.." and date "..pkgdate)
        print("** Check version and date in version.lua then run l3build tag")
      end

      -- checking .sty files
      local m_pkgname, m_pkgdate, m_pkgversion, m_pkgdesc = string.match( filecontents, r_styheader_expl)
      if m_pkgname ~= nil and m_pkgdate == string.gsub(pkgdate, "-", "/") and m_pkgversion == pkgversion then
        os_message("** Checking version and date in "..file['name']..": OK")
      end
      if (m_pkgname~=nil) and ( m_pkgdate ~= string.gsub(pkgdate, "-", "/") or m_pkgversion ~= pkgversion ) then
        print("** Warning: "..file['name'].." is marked with version "..m_pkgversion.." and date "..m_pkgdate)
        print("** Warning: version.lua is marked with version "..pkgversion.." and date "..pkgdate)
        print("** Check version and date in version.lua then run l3build tag")
      end

      -- checking readme files
      local m_bundlename, m_bundleversion, m_bundledate = string.match( filecontents, r_readme)
      if m_bundlename ~= nil and m_bundledate == string.gsub(pkgdate, "(%d+)%-(%d+)%-(%d+)", "%3.%2.%1") and m_bundleversion == "v"..pkgversion then
        os_message("** Checking version and date in "..file['name']..": OK")
      end
      if (m_bundlename~=nil) and ( m_bundledate ~= string.gsub(pkgdate, "(%d+)%-(%d+)%-(%d+)", "%3.%2.%1") or m_bundleversion ~= "v"..pkgversion ) then
        print("** Warning: "..file['name'].." is marked with version "..m_bundleversion.." and date "..m_bundledate)
        print("** Warning: version.lua is marked with version "..pkgversion.." and date "..pkgdate)
        print("!! Check version and date in version.lua then run l3build tag")
      end

      -- currently, no check for tex-style comment is implemented

    end
end


function ic_automation()
  print("ic_automation()")
  for i, modulepath in pairs(modules) do
    -- print("ic_automation()", modulepath)
    module = string.gsub(modulepath, "config/", "")
    -- print("ic_automation()", module)
    -- create release-ic directory
    dir_structured = builddir.."/ic-structured/"..module
    errorlevel = mkdir(dir_structured)
    if errorlevel ~= 0 then
      error("** Error!!: The directory '"..dir_structured.."' could not be created")
    end
    os_message("** Creating the directory '"..dir_structured.."': OK")

    -- create release-ic-flat directory
    dir_flat = builddir.."/ic-flat/"..module
    errorlevel = mkdir(dir_flat)
    if errorlevel ~= 0 then
      error("** Error!!: The directory '"..dir_flat.."' could not be created")
    end
    os_message("** Creating the directory '"..dir_flat.."': OK")

    -- copy the template files to distribdir/ctan
    cp(
      "*",
      builddir.."/"..module.."/distrib/ctan/"..module,
      dir_flat
    )



    -- through the module name, find out if the current module is a demo or a template by pattern recognition
    local demo = string.match(module, "^(.+)-demo.*$")
    local template = string.match(module, "^(.+)-template.*$")

    -- this is the base class
    if not demo and not template then
      -- copy the template files to distribdir/tds and rename to texmf
      -- print(builddir.."/"..module.."/distrib/tds")
      -- print(dir_structured.."/texmf")
      cp(
        "tds",
        builddir.."/"..module.."/distrib/",
        dir_structured
      )
      os.rename(
        dir_structured.."/tds",
        dir_structured.."/texmf"
      )
    end

    -- if demo is defined, it also contains the base name of the module this demo is showcasing
    if demo then
      -- copy demo files into releases
      errorlevel = cp(
          "*",
          builddir.."/ic-flat/"..demo,
          dir_flat
      )
      if errorlevel ~= 0 then
          error("** Error!!: Copying files from '"..module.."' into '"..dir_flat.."' failed")
      end
      os_message("** Copying files from '"..module.."' into "..dir_flat)

      errorlevel = cp(
          "*",
          builddir.."/ic-structured/"..demo,
          dir_structured
      )
      if errorlevel ~= 0 then
          error("** Error!!: Copying files from '"..module.."' into '"..dir_structured.."' failed")
      end


            -- copy the template files to distribdir/tds and rename to texmf
      cp(
        "*",
        builddir.."/"..module.."/distrib/tds",
        dir_structured
      )
      -- os.rename(
      --   dir_structured.."/tds",
      --   dir_structured.."/texmf"
      -- )
      os_message("** Copying files from '"..module.."' into "..dir_structured)
    end

    -- if template is defined, it also contains the base name of the module this template is showcasing
    if template then
      -- os_message("** Copying files from '"..module.."' into release-ic-flat")
      -- -- copy template files into release
      -- cp(
      --     "*",
      --     builddir.."/release-ic-flat/"..parenttemplate,
      --     builddir.."/release-ic-flat/"..module
      -- )
      -- copy template files into releases
      errorlevel = cp(
          "*",
          builddir.."/ic-flat/"..template,
          dir_flat
      )
      if errorlevel ~= 0 then
          error("** Error!!: Copying files from '"..module.."' into '"..dir_flat.."' failed")
      end
      os_message("** Copying files from '"..module.."' into "..dir_flat)


      errorlevel = cp(
          "*",
          builddir.."/ic-structured/"..template,
          dir_structured
      )
      if errorlevel ~= 0 then
          error("** Error!!: Copying files from '"..module.."' into '"..dir_structured.."' failed")
      end


      -- copy the template files to distribdir/tds and rename to texmf
      cp(
        "*",
        builddir.."/"..module.."/distrib/tds",
        dir_structured
      )
      -- os.rename(
      --   dir_structured.."/tds",
      --   dir_structured.."/texmf"
      -- )

      os_message("** Copying files from '"..module.."' into "..dir_structured)
      -- cp(
      --     "*",
      --     builddir.."/release-ic/"..parenttemplate,
      --     builddir.."/release-ic/"..module.."/texmf"
      -- )
      -- os_message("** Copying files from '"..module.."' into 'release-ic'")
    end

    -- copy readme to release
    errorlevel = cp(
            "README.md",
            maindir.."/src",
            dir_structured
    )
    if errorlevel ~= 0 then
          error("** Error!!: Copying README.md from '"..maindir.."/src' into '"..dir_structured.."' failed")
    end
    errorlevel = cp(
            "README.md",
            maindir.."/src",
            dir_flat
    )
    if errorlevel ~= 0 then
          error("** Error!!: Copying README.md from '"..maindir.."/src' into '"..dir_flat.."' failed")
    end

    -- copy the latexmkrc to tds release
    errorlevel = cp(
        "latexmkrc",
        maindir.."/src/latexmk/structured",
        dir_structured
    )
    if errorlevel ~= 0 then
          error("** Error!!: Copying latexmkrc from '"..maindir.."/src/latexmk/structured' into '"..dir_structured.."' failed")
    end
    errorlevel = cp(
        "latexmkrc",
        maindir.."/src/latexmk/flat",
        dir_flat
    )
    if errorlevel ~= 0 then
          error("** Error!!: Copying latexmkrc from '"..maindir.."/src/latexmk/flat' into '"..dir_flat.."' failed")
    end
  end

  -- rename for release
  os.rename(
    builddir.."/ic-flat",
    builddir.."/"..buildname.."-"..flatname.."-v"..pkgversion
  )
  os.rename(
    builddir.."/ic-structured",
    builddir.."/"..buildname.."-"..structuredname.."-v"..pkgversion
  )

end



-- Add "tagged" target to l3build CLI
if options["target"] == "tagged" then
  check_marked_tags()
  os.exit()
end

-- add "release" target to l3build CLI
if options["target"] == "release" then
  -- Capture output of git commands
  local gitbranch = os_capture("git symbolic-ref --short HEAD")
  --   seemingly reads the (short name) of the symbolic reference HEAD
  --  so, this is the currently checked-out branch name


  local gitstatus = os_capture("git status --porcelain")
  --    gitstatus is seemingly an array of status codes and files


  local tagongit  = os_capture('git for-each-ref refs/tags --sort=-taggerdate --format="%(refname:short)" --count=1')
  -- seemingly lists the most recent (local!) tag
  -- consider fetching all of them beforehand using: git fetch --tags

  local gitpush   = os_capture("git log "..gitbranch.." --not --remotes")
  --   lists all commits, which are local but not remote. In particular, lists all unpushed local commits in other branches: this prevents "forgetting" local branches, but also means, that you cannot have local branches for continued development. For now, I will not use this feature


  --   ensure the build process is started from the main branch
  --   should work with all common names for main branches
  if gitbranch == "master" or gitbranch == "main" then
    os_message("** Checking git branch '"..gitbranch.."': OK")
  else
    error("!! Error: You must be on the 'master' or 'main' branch")
  end

  --   ensure there are no uncommited changes, i.e. the main branch is not dirty
 if gitstatus == "" then
    os_message("** Checking git status : OK")
  else
    error("!! Error: Files have been edited, please run 'git add .' and 'git commit'")
  end

  --   ensure there are no local changes (in this brnach) to push to remote
   if gitpush == "" then
    os_message("** Checking pending commits: OK")
  else
    error("!! Error: There are pending commits, please run 'git push'")
  end


  -- check, if all files are tagged correctly (according to pkgname and pkgdate)
  -- if not, throw an error
  check_marked_tags()


  local pkgversion = "v"..pkgversion

  os_message("   Checking last tag marked on remote "..tagongit..": OK")
  errorlevel = os.execute("git tag -a "..pkgversion.." -m 'Release "..pkgversion.." "..pkgdate.."'")
  if errorlevel ~= 0 then
    error("!! Error: tag "..tagongit.." already exists, run 'git tag -d "..pkgversion.." && git push --delete origin "..pkgversion.."'")
    return errorlevel
  else
    os_message("   Running: git tag -a "..pkgversion.." -m 'Release "..pkgversion.." "..pkgdate.."'")
  end

  -- push newly created tag to git remote
  os_message("   Running: git push --tags --quiet")
  os.execute("git push --tags --quiet")

  --
  os_message("Creating release")
  os.execute("l3build ctan > "..os_null)

  --   TODO: automated copying according to ic rules
  ic_automation()

  print("!! Now check "..builddir.."/release for the finished build")
  print("!! If everything is OK, copy 'release' (manually) to desired destination")

  os.exit(0)
end

-- add "dev" target to l3build CLI
if options["target"] == "dev" then
  -- check, if all files are tagged correctly (according to pkgname and pkgdate)
  -- if not, throw an error
  check_marked_tags()

  -- do NOT push the tags into repository, this is devrelease

  os_message("Creating development release")
  os.execute("l3build ctan > "..os_null)

  --   TODO: automated copying according to ic rules
    ic_automation()

  print("!! Now check "..builddir.."/release for the finished build")
  print("!! If everything is OK, run 'l3build release'")

  os.exit()
end
