# ``ic-templates``

*This readme file is only concerned with the ``ic-templates`` project. It does **not** address the particular classes or their usage.*

This project contains the source files and build configuration necessary to build the ``ic-template`` releases.

## Getting started
1. Ensure that you have ``l3build`` installed on your system by running
    ~~~bash
    l3build --version
    ~~~
    to produce an output similar to
    ~~~text
    l3build: A testing and building system for LaTeX

    Release 2024-01-09
    Copyright (C) 2014-2024 The LaTeX Project
    ~~~

2. To build a full release of all contained templates, run
    ~~~bash
    l3build release
    ~~~
    from the project root.
    ``l3build`` will read its configutaion from ``build.lua`` in the project root, and build the target *release-ic*.
    This project configuration file defines the *modules*, which form this bundle.
    During this build process, additional build processes are spawned for each module, which load the respective configuration files ``config/<module name>/build.lua``.
    The build process is lengthy and will take some time.
3. The artefacts of the build process reside in ``<project root>/build`` inside their respective module directories.
    Additional directories ``release-ic`` and ``release-ic-flat`` are being created and filled with the final artefacts.
    ``release-ic`` contains a tds structure of demos and templates, while a flat project structure is contained in ``release-ic-flat``.
    Both share the same contents and can be used equally, depending on personal taste.

## Issues
It is only natural for a piece of software, that issues or incompatibilities appear.
If you encounter an unexpected situation or straight-up error, use the issue tracker of this git repository to tell the maintainer(s).

## Development
There are additional build target available for development:
* ``l3build tagged`` checks, if all files are tagged correctly according to the specifications in ``version.lua``.
* ``l3build tag`` applies the tagging update and updates versions and dates according to ``version.lua``.
* ``l3build dev`` produces a development release, skipping the git checks
* ``l3build release`` does the full release cycle:
    1. using git to ensure the local version is equal to the remote version
    2. using git to check that the local version is on ``main`` branch
    3. using git to tag the version with the tag from ``version.lua``
    4. push tag to remote to provide consistent reference between releases and git commits
    5. use ``l3build ctan`` to build all the submodules
    6. uses custom automatisation to copy files into the desired release structure.

### Release process
* run ``l3build dev`` and make changes until satisfied
* once satisfied, update ``version.lua`` to bump the version number and release date
* run ``l3build tag`` to apply the version numbering to all source files
* create the release commit
* push the release commit to the remote ``git push origin``
* ensure you are on branch ``main``
* run ``l3build release`` to create the release, tag it and push the tag to git remote
* manually copy the release to desired locations

### Add aditional class or package
* create module sources in ``src/<module name>``
* create build configuration in ``config/<module name>/build.lua``. If in doubt, copy from ``ic-book`` or ``ic-thesis``.
* tell ``build.lua`` about your module: edit variable ``modules``.

### Add additional demo or template
* create it as a package: take special note, that to be associated with a class or package module ``<parent module>``, the name must take the form ``<parent module>-<your module name>``, where ``<your module nanme>`` **must not** contain dashes ``-``.
* take special care, that the build configuration is different for demos and templates, as it actually is a misuse of the ``l3build``-system. If in doubt, copy from ``ic-book-demo1`` or ``ic-thesis-template``.
