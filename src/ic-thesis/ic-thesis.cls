% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%
% %% This file is part of the ic-templates bundle developed for the Chair of Intelligent Control Systems.
% %% For additional information visit https://git-ce.rwth-aachen.de/marc.weber/ic-templates
% %% Release Version  : 0.9.19
% %% Release Date     : 2024-08-24
% %%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load expl3 language requirements
% NOTE: these need to be loaded before the class starts
\RequirePackage{expl3}
\RequirePackage{l3keys2e}

\NeedsTeXFormat{LaTeX2e}[2022-06-01]
\ProvidesExplClass {ic-thesis} {2024/08/24} {0.9.19} {Thesis Class for the Chair of Intelligent Control Systems}


%% Load base class
% general development paradigm: load the packages with as few options as possible here. If options needs to be given, define them in ``preloading.dtx'' using ``\PassOptionsToPackage{<option>}{<package>}''. Preferrably, you should use the package's own setup macro.

% additional arguments needs to be added using \PassOptionToClass prior to loading the base class and belong into 'preloading.dtx'
\LoadClassWithOptions{ic-book}

% setup a sequence variable to store the supervisors
\seq_clear_new:N \g_supervisor__supervisors_seq

% overwrite the \maketitle: implementation from ic-book
\input{ic.fcn.maketitle.implementation-layer.clo}

% add the supervisor command (just like \author)
\input{ic.fcn.supervisor.document-layer.clo}
\input{ic.fcn.supervisor.implementation-layer.clo}




% add additional translations specific to the ic-thesis template
\ExplSyntaxOff
\DeclareTranslation{English}{template_facultyinfo_str}{
    The present thesis has been submitted to the {Chair~of~Intelligent~Control~Systems} at the {Faculty~of~Electrical~Engineering~and~Information~Technology} of {RWTH~Aachen~University}.
  }
\DeclareTranslation{German}{template_facultyinfo_str}{
    Diese Arbeit wurde eingereicht am {Lehrstuhl~für~Intelligente Regelungssysteme} an der {Fakultät~für~Elektrotechnik~und~Informationstechnik} der {RWTH~Aachen~University}.
  }
\DeclareTranslation{English}{supervisor}{
    Supervisor
  }
\DeclareTranslation{German}{supervisor}{
    Betreuer
  }
\DeclareTranslation{English}{supervisors}{
    Supervisors
  }
\DeclareTranslation{German}{supervisors}{
    Betreuer
  }
\ExplSyntaxOn

\endinput
