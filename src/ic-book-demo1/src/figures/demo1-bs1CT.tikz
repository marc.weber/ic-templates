\begin{tikzpicture}

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%    Requirements                                %%%%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	% Packages
	% ------------------------------------------------------
	% \RequirePackage{tikz}
	% \RequirePackage{tkz-euclide}
	% \usetikzlibrary{positioning}
	% \usetikzlibrary{math}

	% Styles
	% ------------------------------------------------------
	% system, sumpoint, branchpoint
	% lineStyle:signal (+arrow)

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%    Parameters                                  %%%%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	\tikzmath{%
		real \x, \smallX, \y;%
		\x = 1.2;%
        \smallX=0.75*\x;%
		\y = 1;%
	}%

	\tikzset{%
		every node/.append style={%
            % on grid,%
            node distance = \y and \x,%
        },%
	}%

    \tikzset{%
        systemWide/.append style={%
            system,%
            align=center,%
            minimum width=2cm,%
        },%
    }%

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%    Figure                                      %%%%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	% Nodes
	% ------------------------------------------------------
    \node[sumpoint] (sum) at (0,0) {};
    \node[anchor=north west, inner sep=0] at (sum.south east) {\( - \)};

    \node[systemWide, right=of sum] (regler) {zeitkont.\\Regler};
    \node[systemWide, right=of regler] (strecke) {zeitkont.\\Strecke};

    \coordinate[branchpoint, right=\smallX of strecke] (branch);


	% In-/outputs and tmp coordinates
    % ------------------------------------------------------
    \coordinate[left=\smallX of sum] (input);
    \node[anchor=south west, inner xsep=0] at (input) {\( r(t) \vphantom{y} \)};

    \coordinate[right=\smallX of branch] (output);
    \node[anchor=south east, inner xsep=0] at (output) {\( y(t) \vphantom{y} \)};

    \coordinate[below=of strecke] (tmpFeedback);

	% Signals
	% ------------------------------------------------------
    \draw[lineStyle:signal, ->] (input) -- (sum);
    \draw[lineStyle:signal, ->] (sum) -- (regler) node[midway, above] {\( e(t) \vphantom{y} \)};
    \draw[lineStyle:signal, ->] (regler) -- (strecke) node[midway, above] {\( u(t) \vphantom{y} \)};
    \draw[lineStyle:signal, ->] (strecke) -- (output);
    \draw[lineStyle:signal, ->] (branch.center) |- (tmpFeedback) -| (sum);

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%    Debugging                                   %%%%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{tikzpicture}