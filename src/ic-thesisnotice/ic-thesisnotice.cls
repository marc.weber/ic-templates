% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%
% %% This file is part of the ic-templates bundle developed for the Chair of Intelligent Control Systems.
% %% For additional information visit https://git-ce.rwth-aachen.de/marc.weber/ic-templates
% %% Release Version  : 0.9.19
% %% Release Date     : 2024-08-24
% %%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Load expl3 language requirements
% NOTE: these need to be loaded before the class starts
\RequirePackage{expl3}
\RequirePackage{l3keys2e}

\NeedsTeXFormat{LaTeX2e}[2022-06-01]
\ProvidesExplClass {ic-thesisnotice} {2024/08/24} {0.9.19} {Thesis Notice Class for the Chair of Intelligent Control Systems}


%% Variable Definitions

%% Option Definitions

% \DeclareKeys is the future way of declaring key-value pairs and option handling. An alternative with a superset of options would be to use \keys_define:nn directly. However, for now \DeclareKeys is a sufficient choice and supersedes the ancient option declaration mechanism by far.
% \DeclareKeys has been introduced into the LaTeX language on 21.03.2022, see the following announcement:
%   https://www.latex-project.org/news/2022/03/21/latex-dev-2022-1/
% Documentation is sparse as of writing, but the following will help:
%   https://tex.stackexchange.com/a/648001/53868
%   https://ftp.tu-chemnitz.de/pub/tex/macros/latex/base/source2e.pdf (see ltxkeys.dtx)
\keys_define:nn {l3icnotice} {
    % the fontsize option my ba set as the class option using
    %   fontsize = Xpt
    % where X my be any positive number
    % or alternatively through the meta keys by just specifying Ypt,
    % where Y is one of the following defined meta keys:
    fontsize  .dim_gset:N = \g_icnotice__fontsize_dim,
    fontsize  .initial:n  = 10pt,
    9pt       .meta:n     = {fontsize=9pt},
    10pt      .meta:n     = {fontsize=10pt},
    11pt      .meta:n     = {fontsize=11pt},
    12pt      .meta:n     = {fontsize=12pt},
    14pt      .meta:n     = {fontsize=14pt},
    17pt      .meta:n     = {fontsize=17pt},
    20pt      .meta:n     = {fontsize=20pt},
    25pt      .meta:n     = {fontsize=25pt},
    30pt      .meta:n     = {fontsize=30pt},
    36pt      .meta:n     = {fontsize=36pt},
    48pt      .meta:n     = {fontsize=48pt},
    60pt      .meta:n     = {fontsize=60pt},
    % override default option for DIC size in scrartcl
    % scrbook uses a grid to divide the page into typearea and margins. A default sizing is to divide this grid into 9 divisions. For a more detailed explanation, have a look at the documentation of scrbook and typearea in scrguide.
    DIV       .code:n     = {\PassOptionsToClass{DIV = #1}{scrartcl}},
    DIV       .initial:n  = 17,
    DIV       .value_required:n = true,
    % override default paper size to be a4 paper in scrbook
    paper     .code:n   = {\PassOptionsToClass{paper = #1}{scrartcl}},
    paper     .initial:n = a4,
    paper     .value_required:n = true,
  }


% \ProcessKeysOptions also handles global "class" options. This is the intended way, as we want to react to global changes e.g. language selections
%  see: https://tex.stackexchange.com/a/371754/53868
% process options for this class
\ProcessKeysOptions{l3icnotice}



%% Pre-Package-Loading:

% setup the base class
\PassOptionsToClass{
    % paper = a4paper,
    % oneside,
    mpinclude=true,
    usegeometry=true,
    % DIV=17,
  }{scrartcl}

% Set the default color model of package xcolor to rgb, as the main use will be to view documents on screens.
% TODO: in the future and if wanted, we shouldimplement a solution for using CMYK, HKS and Pantone colours for print
\PassOptionsToPackage{rgb}{xcolor}

% For package 'enumitem', pass options for inline enumerations and?
\PassOptionsToPackage{shortlabels}{enumitem}
\PassOptionsToPackage{inline}{enumitem}


%% Load base class
% general development paradigm: load the packages with as few options as possible here. If options needs to be given, define them in the ``%% Pre-Package-Loading`` section using ``\PassOptionsToPackage{<option>}{<package>}''. Preferrably, you should use the package's own setup macro.
\LoadClassWithOptions{scrartcl}

% in order to supress multiple warnings regarding \float@addtolist in conjunction with \chapter, floatrow et al. load this package as a workaround. KOMAScript author Markus Kohm is working on a package floatrowbytocbasic(2023), but this did no resolve the issue with \chapter for now.
% TODO: possibly replace this in the future.
\RequirePackage{scrhack}

%% Load Packages: Typography
% Load a sans-serif font that resembles the RWTH house font HelveticaNeueLTCom or daily font Arial: They both are commercial fonts and in general not available to TeX compilers.
% The only possibiliuty is to load them from the TrueType font files, which mandates the use of package fontspec, which mandates the use on LaTeX engine lualatex or xetex and was not allowed by CE. If this opinion changes in the future, I recommend to make the switch to HelveticaNeueLTCom
\RequirePackage{ClearSans}
\def\sfdefault{\clearfamily}

% fontawesom for pictograms
\RequirePackage{fontawesome}

% load the typographic enhancements needed for precice optical alignment. No commands from this package are used directly, it is just optimizing kerning.
\RequirePackage{microtype}

% enumitem replaces the default enumerate itemize and description environments by their more capable enumitem counterparts. The default syntax is kept and only extended by additioal optional arguments.
\RequirePackage{enumitem}

%% Load Packages: visuals and page layouting
% requirement for customizing the base KOMA-Script, i.e. scrbook
\RequirePackage{scrlayer-scrpage}
% a notecolumn is used to typeset the additional visuals
\RequirePackage{scrlayer-notecolumn}
% Load the geometry package for resizing margins: this is necessary to create the Schmutztitel (otherwise, this would need to be done in TikZ, which obviously is always an option for me)
\RequirePackage{geometry}

% Package graphicx is required for including external pdf files. It is mandatory for typesetting the chair logo.
\RequirePackage{graphicx}

% Package tikz is required for sensibly typesetting the logo in the header and is helpful throughout most documents
\RequirePackage{tikz}

% Package pgfplots is required to render some of the visual diagrams in the side column
\RequirePackage{pgfplots}


%% Load Packages: i18n
% babel is a package for setting up languages for the default document titles, e.g. using babel, the title displayed over \tableofcontents will say "Contents" for documents setup in English, while it will say "Inhaltsverzeichnis" for documents typeset in German. Also, labels for figures, tables, etc. are bing translated.
\RequirePackage{babel}

% On some instances, text needs to be translated into the respective language. The translator package provides this option and was the preferred choice, as it also is used inside the beamer package.
% INFO: translator package is not good enough, so I switched to the better translations package
% \RequirePackage{translator}
\RequirePackage{translations}

%% Load Packages: colors
% use package xcolor to define and redefine colours throughout documents. Package is necessary for Schmutztitel and very common for use with a lot of different packages.
\RequirePackage{xcolor}
\input{ic.colors.clo}

%% Load Packages: floating environments
% package used for having sub-figures in figures, i.e. figures belonging together, but having their own name to refer
\RequirePackage{caption}
\RequirePackage{floatrow}
\RequirePackage{subcaption}

% use placeins package to fine-tune placement of floats
\RequirePackage{placeins}

%% Load Packages: common mathamatic commands
% since we mainly focus in mathematical subjects, loading of required environments and symbols is needed. amssymb loads math fonts and special symbols, amsmath loads the general equation environments. Some special symbols are not present: to have \coloneqq and \eqqcolon (the definition operator), we need mathtools. To display physical units adequately (mostly in technical docuumentations), we recommend package siunitx to adhere to the SI notation standard. Laplace operators are typeset using the mathrsfs package. To typeset the differential operator, e.g. in integrals, use \dd{\tau} from the physics package, which also defines a consistent way for typesetting partial derivatives correctly.
\RequirePackage{amssymb}
\RequirePackage{amsmath}

% mathtools provides lots of useful extensions to default amsmath symbols, macros and environments.
% For a more detailed explanation, consider the mathtools documentation.
% TODO: cosider using option 'showonlyrefs', but can be used in customization by \mathtoolset{showonlyrefs=true|false}
\RequirePackage{mathtools}

% Package siunitx provides a consistent way to typeset numbers and units independently from a chosen locale.
% This means, that \SI{9.81}{\metre\per\kilogram\second^2} will typeset commata and 1000-separation points with respect to the language chosen for the document either as a comma or as a period.
% use \num[]{23.5} to display numbers in suitable locale formats (automatically switches between . and , depending on selected language, can format large numbers automatically, can give error margins)
% use \si[]{\metre} do display units alone
% use \SI[]{9.81}[]{\newton\per\kilogram} to display numbers WITH units
% for additional options see the comprehensive siunitx documentation
\RequirePackage{siunitx}

% additional font required for suitable Laplace operator symbol
% use \mathscr{L}
\RequirePackage{mathrsfs}

% interval package allows a consistent display of mathematical intervals (open, closed).
% Interval delimiters may be changed at a later point in a customized preamble, to adhere to the document author's taste or publishing requirements.
\RequirePackage{interval}

% typeset differentials and derivative operations in a structured way
\RequirePackage{derivative}


%% Load Packages: Theorems
% TODO: consider removing amsthm and thmtools in favour of more advanced theorem techniques, which don't use \item: the document-author interface should stay the same, though.
% amsthm allows the usage of theorem environments, like theorem or proofs. thmtools allows for easier customization and definition of new environments, such as example, lemma, proposition.
\RequirePackage{amsthm}
% thmtools is a wrapper for amsthm which allows easier creation of theorem-like environments.
\RequirePackage{thmtools}

%% Load Packages: referencing
\RequirePackage{hyperref}
\RequirePackage{cleveref}


%  TODO: check sans serif font
%  TODO: copy genuine content from dennis

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Setup fonts

% redefine font sizes to match memoir class (KOMAScript is a bit less consistent)
% NOTE: workaround to reuse the memoir fontsize definitions
\newcommand*{\@ivpt}{4}
\newcommand*{\@xxxpt}{30}
\newcommand*{\@xxxvipt}{36}
\newcommand*{\@xlviiipt}{48}
\newcommand*{\@lxpt}{60}
\newcommand*{\@lxxiipt}{72}
\newcommand*{\@lxxxivpt}{84}
\newcommand*{\@xcvipt}{96}
\newcommand*{\@cviiipt}{108}
\newcommand*{\@cxxpt}{120}
\newcommand*{\@cxxxiipt}{132}
\newlength{\onelineskip}
\newlength{\lxvchars}
\newlength{\xlvchars}
\newif\ifextrafontsizes
\extrafontsizestrue

\dim_case:nnF { \g_icnotice__fontsize_dim }
  {
    {9pt} {\input{mem9.clo}}
      {10pt} {\input{mem10.clo}}
      {11pt} {\input{mem11.clo}}
      {12pt} {\input{mem12.clo}}
      {14pt} {\input{mem14.clo}}
      {17pt} {\input{mem17.clo}}
      {20pt} {\input{mem20.clo}}
      {25pt} {\input{mem25.clo}}
      {30pt} {\input{mem30.clo}}
      {36pt} {\input{mem36.clo}}
      {48pt} {\input{mem48.clo}}
      {60pt} {\input{mem60.clo}}
  }{
    % default?
    \input{mem10.clo}
  }

% define font series switches. These must not be used directly in documents, as they are SWITCHES and not commands.
\DeclareRobustCommand{\elseries}{\fontseries{el}\selectfont}
\DeclareRobustCommand{\lseries}{\fontseries{l}\selectfont}
\DeclareRobustCommand{\ltseries}{\fontseries{lt}\selectfont}
\DeclareRobustCommand{\ltxseries}{\fontseries{ltx}\selectfont}
\DeclareRobustCommand{\bxseries}{\fontseries{bx}\selectfont}
\DeclareRobustCommand{\bcseries}{\fontseries{bc}\selectfont}
\DeclareRobustCommand{\mdcseries}{\fontseries{mdc}\selectfont}
\DeclareRobustCommand{\mdxseries}{\fontseries{mdx}\selectfont}
% \DeclareRobustCommand{\tseries}{\fontseries{t}\selectfont}

% make sans-serif the default font family
\renewcommand{\familydefault}{\sfdefault}
% make extra-light the default font series
\renewcommand{\seriesdefault}{l}
% sans-serif paragraphs should not be indented
\setlength{\parindent}{0pt}

% (Re-)Set fonts for KOMA-Script defined sectioning
\setkomafont{subject}{\sffamily\Large\color{blue}}
\setkomafont{title}{\HUGE\sffamily\bfseries\color{blue}}
\setkomafont{section}{\LARGE\sffamily\mdxseries\color{blue}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% setup enumerations
% Update the default enumeration environments to use a consistend classic numbering scheme and use less space
\setlist[itemize,1]{
    label={·},
    labelsep=2.5mm,
    leftmargin=*,
  }
\setlist[itemize,2]{label={·}, labelsep=2.5mm, leftmargin=*}
\setlist[enumerate,1]{
    label={\arabic*.},
    font=\footnotesize,
    labelsep=2.5mm,
    leftmargin=*,
    %topsep=2cm,
  }
\setlist[enumerate,2]{
    label={\roman*.},
    font=\footnotesize,
    labelsep=2.5mm,
    leftmargin=*
  }
\setlist[description,1]{labelindent=1em}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Setup: visuals and graphics
% load mandatory tikz libraries
\ExplSyntaxOff
\usetikzlibrary{backgrounds}
\usetikzlibrary{positioning}
\usetikzlibrary{calc}
\usetikzlibrary{intersections}
\usetikzlibrary{patterns, angles, quotes}
\usetikzlibrary{babel}
\usetikzlibrary{fadings,decorations.pathreplacing}
\usetikzlibrary{arrows}
\usetikzlibrary{arrows.meta}
\ExplSyntaxOn


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Setup: floating environments
% Configure the default behaviour for figure captions
\captionsetup[figure]{
    font=smaller,
  }
\captionsetup[subfigure]{
    font=smaller,
    singlelinecheck=off,
    justification=raggedright,
  }




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Setup: mathamatic symbols
% configure intervals to use the default notation
\intervalconfig{
    left ~ open ~ fence = \lparen,
    right ~ open ~ fence = \rparen
  }

%% configure the derivative package for a common base notation
\derivset{\pdv}[
    delims-eval=.|,
  ]
\derivset{\odv}[
    delims-eval=.|,
  ]

% commonly used named sets
\DeclareDocumentCommand \R {} {\ensuremath{\mathbb{R}}}
\DeclareDocumentCommand \C {} {\ensuremath{\mathbb{C}}}
\DeclareDocumentCommand \N {} {\ensuremath{\mathbb{N}}}
\DeclareDocumentCommand \Z {} {\ensuremath{\mathbb{Z}}}
\DeclareDocumentCommand \Q {} {\ensuremath{\mathbb{Q}}}

\input{ic.operators.document-layer.clo}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Setup: notice-specific symbols
% \newcommand{\emailsymbol}{\faAt}
\NewDocumentCommand{\emailsymbol}{}{{\hbox to 1em{\hfil\faAt\hfil}}}
\NewDocumentCommand{\phonesymbol}{}{{\hbox to 1em{\hfil\faPhone\hfil}}}
\NewDocumentCommand{\homepagesymbol}{}{{\hbox to 1em{\hfil\faChain\hfil}}}
\NewDocumentCommand{\locationsymbol}{}{{\hbox to 1em{\hfil\faMapMarker\hfil}}}
\NewDocumentCommand{\institutsymbol}{}{{\hbox to 1em{\hfil\faBuilding\hfil}}}
\NewDocumentCommand{\linkedinsymbol}{}{{\hbox to 1em{\hfil\faLinkedin\hfil}}}
\NewDocumentCommand{\twittersymbol}{}{{\hbox to 1em{\hfil\faTwitter\hfil}}}
\NewDocumentCommand{\githubsymbol}{}{{\hbox to 1em{\hfil\faGithub\hfil}}}
\NewDocumentCommand{\orcidsymbol}{}{{\hbox to 1em{\hfil\aiOrcid\hfil}}}
\NewDocumentCommand{\mailsymbol}{}{{\hbox to 1em{\hfil\faEnvelope\hfil}}}

\NewDocumentCommand{\ratingmarker}{}{\faCircle}


















%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Setup: Layout

% load trhe maketitle implementation
\input{ic.fcn.@maketitle.implementation-layer.clo}
\input{ic.fcn.maketitle.implementation-layer.clo}
\input{ic.fcn.maketitle.document-layer.clo}


% setup the page margins
\newgeometry{
    left=1cm,
    right=1cm,
    top=1.25cm,
    bottom=1.25cm,
    footskip=2\baselineskip
  }

% set pagestyle for the whole document to plain, i.e. remove page numbers
\pagestyle{empty}
% override titlepage style for scrartcl
\RenewDocumentCommand{\titlepagestyle}{}{empty}
% \renewcommand*{\titlepagestyle}{empty}

% allow additional spacing for text to not extend into the margins
% https://tex.stackexchange.com/a/9110/53868
\emergencystretch 3em


% format the sectioning lines to provide the shown blue lines underneath
\RenewDocumentCommand{\sectionlinesformat}{ m m m m }{
 \ifstr{#1}{section}{%
     \parbox[t]{\linewidth}{
         \raggedsection\@hangfrom{\hskip #2#3}{#4}\par
         \color{black75}
         \kern-.75\ht\strutbox\rule{\linewidth}{0.75pt}
       }
   }{
     \@hangfrom{\hskip #2#3}{#4}
   }
 }

 % split the whole document body into two columns using notecolumns:
 % the split takes the whole available textwidth and splits it according
 \newlength{\paragraphscolwidth}
 \setlength{\marginparsep}{0.6cm}
 \setlength{\paragraphscolwidth}{%
     .382\textwidth}%
 \addtolength{\paragraphscolwidth}{%
     -\marginparsep}%

 \DeclareNewNoteColumn[%
     position=\oddsidemargin+1in
     +.618\textwidth
     +\marginparsep,
     width=\paragraphscolwidth,
   ]{sidebar}





 \str_case_e:nnF { \languagename }
   {
     {german} {\str_gset:Nn \g_language_str {de}}
       {ngerman} {\str_gset:Nn \g_language_str {de}}
   }
   {
     % default language
     \str_gset:Nn \g_language_str {en}
   }


 % otherwise use the default blue version
 % \str_set:Nx \l_logoname_str {rwth_ic_  \l_logoname_language_str  _rgb}

 % load fontspec for using custom RWTH font HelveticNeueLTCom
 % NOTE: this packages mandates the use of the lualatex engine
 % \RequirePackage{fontspec}

 % NOTE: if available, HelveticaNeueLT is preferrable
 % \usepackage[light]{roboto}

 % \newfontfamily\HNLight[
 % 	% Extension = .ttf,
 % 	% Scale = MatchLowercase,
 % 	FontFace = {lt}{n}{*-Th},
 % 	FontFace = {ltx}{n}{*-ThEx},
 % 	FontFace = {mdc}{n}{*-LtCn},
 % 	FontFace = {md}{n}{*-Lt},
 % 	FontFace = {mdx}{n}{*-LtEx},
 % 	FontFace = {b}{n}{*-Bd},
 % 	FontFace = {bx}{n}{*-BdEx},
 % 	FontFace = {bc}{n}{*-BdCn},
 % 	% BoldItalicFont = HelveticaNeue-UltraLightItalic
 % ]{HelveticaNeueLTCom}



 % \setsansfont{HelveticaNeueLTCom}[
 % 	Scale = MatchLowercase,
 % 	FontFace = {lt}{n}{*-Th},
 % 	FontFace = {ltx}{n}{*-ThEx},
 % 	FontFace = {mdc}{n}{*-LtCn},
 % 	FontFace = {md}{n}{*-Lt},
 % 	FontFace = {mdx}{n}{*-LtEx},
 % 	FontFace = {b}{n}{*-Bd},
 % 	FontFace = {bx}{n}{*-BdEx},
 % 	FontFace = {bc}{n}{*-BdCn},
 % ]
 % \setmainfont{HelveticaNeueLTCom}[
 % 	Scale = MatchLowercase,
 % 	FontFace = {lt}{n}{*-Th},
 % 	FontFace = {ltx}{n}{*-ThEx},
 % 	FontFace = {mdc}{n}{*-LtCn},
 % 	FontFace = {md}{n}{*-Lt},
 % 	FontFace = {mdx}{n}{*-LtEx},
 % 	FontFace = {b}{n}{*-Bd},
 % 	FontFace = {bx}{n}{*-BdEx},
 % 	FontFace = {bc}{n}{*-BdCn},
 % ]
 % \let\normalfont=\HNLight

 % \renewcommand{\rmdefault}{qag} % Arial
 % \renewcommand{\sfdefault}{qag} % Arial
 % \ExplSyntaxOff

 % \ExplSyntaxOn


 \input{ic.fcn.noticetag.document-layer.clo}
 \input{ic.fcn.noticeskill.document-layer.clo}
 % Unresolvable problems with this piece of foreign code: I can not port this to expl3
 \ExplSyntaxOff
 \input{ic.fcn.wheelchart.document-layer.clo}
 \ExplSyntaxOn



 \endinput
