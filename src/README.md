# Release Information
The current version of bundle ``ic-templates`` is ``v0.9.19``, released on ``24.08.2024``.


# Template Bundle ``ic-*``

This template bundle is intended to be used at the *Chair of Intelligent Control Systems*.
The following classes, are contained:
* ``ic-book`` for longer documents, e.g. lecture notes, course instructions, technical documentation, research reports.
* ``ic-thesis`` for theses, e.g. Bachelor Theses and Master Theses. This template is an extension to template ``ic-book`` and inherits from it.
* ``ic-thesis-notice`` for a one-page notification to announce a thesis topic.
* ``ic-exercise`` for exercise sheets.
* (planned) ``ic-exam`` for exams.

Alongside, pre-made **templates** and **demos** showcase the use of the classes.


## Usage
The template comes as an archive ``ic-book-default.zip`` with the following structure.

Notable files:
* ``demo0*`` are the actual template. You should build your own project using these files.
* ``demo1*`` demonstrates a projects with common functionality usinng ``ic-book``.
* ``demo2*`` demonstrates *embedding* an existing document, e.g. the official RWTH styleguide, into the ``ic-book`` template.
* If you prefer to follow the *principle of separation between source code and artifacts*, there is a second archive ``ic-book-default.tds.zip`` inside, which contains a TDS and ``latexmkrc`` to automate the build process.
    The TDS contains the aforementioned example projects inside the ``doc`` subdirectories:
    + ``template`` are the actual template. You should build your own project using these files.
    + ``demo1`` demonstrates a projects with common functionality usinng ``ic-book``.
    + ``demo2`` demonstrates *embedding* an existing document, e.g. the official RWTH styleguide, into the ``ic-book`` template.

### local compilation
Let ``X`` be the demo project you want to compile.
1. From ``ic-book-default.zip``, unpack **only** the directory ``ic-book-default`` anywhere on your hard drive.
2. Delete all files, whose filenames begin with ``demoY*``, where ``Y`` is not you choice of  ``X``. This removes all other demo files from your project.
3. Ensure that the demo compiles by running
    ~~~bash
    pdflatex demoX
    ~~~

#### Remarks:
* You need at least two runs to successfully compile the whole document.
* You may also use ``lualatex`` or ``luahbtex`` as compiler.

### local compilation (using latexmk)
If you use the TDS version, compilation can be done with a single call of ``latexmk`` from your project root directory.
All configuration is done inside ``latexmkrc``, adjust these settings as necessary.
``latexmk`` automatically detects the required number of runs for the main document and dependencies.

Let ``project`` be the demo project of your choice, e.g. ``template``, ``demo1``, ``demo2``.
1. Extract directory ``ic-book-default.tds.zip/doc/latex/ic-book-default/project``  anywhere on your local machine  and rename it to ``<your project directory>``.
2. Create a directory ``<your project directory>/texmf/``
3. Extract the **``ic-book-default.tds.zip/tex``** directory, such that the path is ``<your project directory>/texmf/tex/...``.
4. Ensure that the demo compiles by running
    ~~~bash
    latexmk
    ~~~
    from ``<your project directory>``.

#### Remarks
* If you need to force recompilation despite no change has occurred, force it using ``latexmk -g``.
* Output artifacts reside in ``<your project directory>/build``.
* Temporary artifacts reside in ``<your project directory>/tmp``.

### remote compilation using overleaf
You may use overleaf to compile the template or demos.
As with the local versions, there are two ways to use overlaf as a flat file structure or as TDS.
1. Perform **either** of these preparation steps:
    *  Prepare a local project directory as in section [local compilation](#local-compilation).
    * Prepare a local project directory as in section [local compilation (using latexmk)](#local-compilation-using-latexmk).
2. Pack ``<your project directory>`` as ``<your project directory>.zip``.
3. On overleaf, select **New Project** and select **Upload Project** from the dropdown menu. Drag and drop your ``<yor project directory>.zip`` into the dialogue.
4. Once overleaf has unpacked the archive, it should immediately start to compile the root file.

## Advanced Configuration

### RWTH fonts (t.b.d)
It is advised to replace the default sans-serif font by the official RWTH font **HelveticaNeueLTCom**.
This requires loading the fonts from TrueType font files (either local or preinstalled on your OS) by use of latex package ``fontspec``.
The ``fontspec`` package in turn requires the compiler to be ``lualatex`` or its successor ``luahbtex``.
To use the official RWTH fonts:
* copy fonts into ``<TDS font directory>``
* change the compiler to ``lualatex``: either
    + (local, flat): call ``lualatex`` from your console/editor
    + (local, TDS): set the respective parameter ``$pdf_mode = 4;`` in ``latexmkrc``
    + (remote, overleaf): T.B.D.
* add the following customizations to your preamble:
    ~~~latex
   % in customization.sty
   ...

   ...
    ~~~

## Development
t.b.d.: add Git repository bugtracker

## Notation

* **distribution**
the collection of scripts, binaries, definitions and documentation that are installed on your computer
typically either of *MikTeX*(Windows only), *TeXlive* (all platforms), *tinyTeX* (very recent development based on TeXlive)
* **module**
a collection of files with a specific purpose, e.g. to implement drawing capabilities (``tikz``).
It is an organizational unit.
* **bundle**
a collection of related modules or other bundles.
It too is an organizational unit.
usually, there is an inter-module-dependency in a bundle.
If a module contains isolated functionality, there is no need for a parent bundle, and we consider bundles consisting of only on module still as a module.
* **engine**
distributions come with multiple "compilers", called an *engine*.
each engine has different capabilities:
    + engine *pdflatex* is fast, but old
    + engine *lualatex* is slower, but has additional scripting capabilities
    + engine *xelatex* i know little about
* **TDS**
a distribution provides the so called *TeX Directory Structure*, a hierarchy of directories, where each file has its own place inside the distribution, depending on its module and bundle
**Example**:
    ~~~text
    texlive (install directory of your distribution)
    |-2023 (texlive splits versions by year)
    | |-texmf-dist ()
    | | |-tex
    | | | |-latex (engine)
    | | | | |-memoir (module)
    | | | | | |-memoir.cls
    | | | | | |-mem10.clo
    | | |-fonts
    | | | |-truetype
    | | | | |-google (bundle)
    | | | | | |-noto (bundle)
    | | | | | | |-noto (module)
    | | | | | | | |-NotoSans-BlackItalic.ttf
    | | | | | | | |-NotoSerif-Regular.ttf
    | | | | | | |-noto-emoji (module)
    | | | | | | | |-NotoEmoji-Regular.ttf
    ~~~
    to find out the location of an existing file in TDS use ``kpsewhich filename.extension``
* **class**
In particular, a module may contain one or more class files ``*.cls``.
A class is a collection of common functionality for creating a specific kind of document, e.g. ``article.cls`` for writing monographs.
* **package**
A module may contain one or more packages ``*.sty``.
A package is a collection of common functionality for pursuing a certain purpose when creating a document, e.g. ``amssymb.sty`` for typesetting mathematical symbols.
* **project**
A directory, where you author your documents.
You can author multiple documents inside the same directory, but it is considered good practice, to have one directory per project.
* **document author**
A possible role the user attains when working with projects.
In the role of a document author, the user is concerned with **creating content** for the document.
Users shall only ask themselves questions like "What is this meant to be?" and **not** "How do I want this to look?".
* **module author**
A possible role the user attains when working with projects.
In the role of a module author, the user is concerned with creating functionality and its depiction.
As a module author, the user should contemplate questions like "How do I want a vector symbol to look like?" or "Which is the font size for heading level 3?".
As scientiests, our overall goal is to spend as little time as possible in the role of a module author.
* **source**
Files in projects are considered source, if they are edited by an author and necessary to create the document, e.g. ``my-great-document.tex``.
* **dependencies**
Files in projects are considered dependencies, if they are static or generated by external programs but necessary to create the document, e.g. ``logo.pdf``, ``my-matlab-exported-data.csv``.
* **artefacts**
Files which are generated by engine runs are called artefacts. There can be temporary artefacts, like ``my-great-document.aux``  and final artefacts like ``my-great-document.pdf``.
Artefacts are not necessary for sharing your project with others and should be removed beforehand.
* **functions**
When working as an author, you encounter calls to functions, or macros(older notion). A function has a signature and a body.
The signature consists of a name, e.g. ``\SI``, a list of arguments and  modifiers ``*{arg1}{arg2}[optarg1]``.
The body consists of the definition, e.g. ``{{\bfseries #1}:#2#3}``.
* **LaTeX 3** vs. **LateX2e**
Since 2012, the current specification of *LaTeX2e* is being reworked and big progress has been made.
LateX3 brings new important features for module authors, in particular:
    + typing
    + numeric operations
    + fundamental data types, e.g. token lists, sequences, dimensions, integers, floats, strings, booleans, dictionaries, boxes, coffins
    + planned future features: fonts, colors
    + operators for fundamental data types
    + better control structure: for loops, foreach, maps, iterators
    + correct scoping of variables
Overall better experience from a programming point, document authors do not notice through separation of *document-layer* for signatures and *implementation-layer* for definitions.

## Concepts
* Overall design principles
    + try to stay consistent with RWTH Styleguide
    + keep modules minimal
    + allow document authors to become module authors and adapt
    + use inheritance: TDS trees are inherited and shadowed: txmf < texmfhome < auxtrees < project texmf < project files
* base: ``KOMAScript``, e.g. ``scrbook.cls``, ``scrartcl.cls``, ``scrreprt.cls``
all functionality from base KOMAScript is kept and can be looked up in the extensive ``scrguide.pdf``.
* custom classes for IC:
    + ``ic-book.cls``: longer documents, lecture scripts, longer reports, based on ``scrbook.cls``
    + ``ic-thesis.cls``: inherited from ``ic-book.cls``, special additions for writing theses
    + ``ic-exercise.cls``: class for writing exercises, based on ``scrartcl.cls``
    + ``ic-thesis-notice.cls``: class for writing thesis notices based on ``scrartcl.cls``
    + ``ic-slides.sty``: package for theming documents using class  ``beamer.cls``
* pre-made template projects as easy starts: Just copy these and start workign as a document author.
* pre-made demo projects to see how it can be used. Do **not** directly use them as template.
* all modules are available in a flat version and in a TDS version.
* bugtracking and development on https://git-ce.rwth-aachen.de/marc.weber/ic-templates
