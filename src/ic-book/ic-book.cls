% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%
% %% This file is part of the ic-templates bundle developed for the Chair of Intelligent Control Systems.
% %% For additional information visit https://git-ce.rwth-aachen.de/marc.weber/ic-templates
% %% Release Version  : 0.9.19
% %% Release Date     : 2024-08-24
% %%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Load expl3 language requirements
% NOTE: these need to be loaded before the class starts
\RequirePackage{expl3}
\RequirePackage{l3keys2e}

\NeedsTeXFormat{LaTeX2e}[2022-06-01]
\ProvidesExplClass {ic-book} {2024/08/24} {0.9.19} {Book Class for the Chair of Intelligent Control Systems}

%% Variable Definitions:

% variable for the base class name for later use
\str_const:Nn  \g_icbook__base_class_str { scrbook }

% token list variable to store content for the covertitle
\tl_new:N \g_icbook__covercontent_tl

% store the (main) document language as the language part of the posix locale string.
\str_new:N \g_language_str







%% Option Definitions:

% \DeclareKeys is the future way of declaring key-value pairs and option handling. An alternative with a superset of options would be to use \keys_define:nn directly. However, for now \DeclareKeys is a sufficient choice and supersedes the ancient option declaration mechanism by far.
% \DeclareKeys has been introduced into the LaTeX language on 21.03.2022, see the following announcement:
%   https://www.latex-project.org/news/2022/03/21/latex-dev-2022-1/
% Documentation is sparse as of writing, but the following will help:
%   https://tex.stackexchange.com/a/648001/53868
%   https://ftp.tu-chemnitz.de/pub/tex/macros/latex/base/source2e.pdf (see ltxkeys.dtx)
\keys_define:nn {l3icbook} {
% the fontsize option my ba set as the class option using
%   fontsize = Xpt
% where X my be any positive number
% or alternatively through the meta keys by just specifying Ypt,
% where Y is one of the following defined meta keys:
fontsize  .dim_gset:N = \g_icbook__fontsize_dim,
fontsize  .initial:n  = 10pt,
9pt       .meta:n     = {fontsize=9pt},
10pt      .meta:n     = {fontsize=10pt},
11pt      .meta:n     = {fontsize=11pt},
12pt      .meta:n     = {fontsize=12pt},
14pt      .meta:n     = {fontsize=14pt},
17pt      .meta:n     = {fontsize=17pt},
20pt      .meta:n     = {fontsize=20pt},
25pt      .meta:n     = {fontsize=25pt},
30pt      .meta:n     = {fontsize=30pt},
36pt      .meta:n     = {fontsize=36pt},
48pt      .meta:n     = {fontsize=48pt},
60pt      .meta:n     = {fontsize=60pt},
% override default option for DIC size in scrbook
% scrbook uses a grid to divide the page into typearea and margins. A default sizing is to divide this grid into 9 divisions. For a more detailed explanation, have a look at the documentation of scrbook and typearea in scrguide.
DIV       .code:n     = {\PassOptionsToClass{DIV = #1}{\g_icbook__base_class_str}},
DIV       .initial:n  = 11,
DIV       .value_required:n = true,
% override default paper size to be a4 paper in scrbook
paper     .code:n   = {\PassOptionsToClass{paper = #1}{\g_icbook__base_class_str}},
paper     .initial:n = a4,
paper     .value_required:n = true,
% pass default value for usage of geometry
usegeometry .code:n = {\PassOptionsToClass{usegeometry = #1}{scrartcl}},
usegeometry .initial:n = {true},
usegeometry .value_required:n = true,
% handle the logo color
% this sets the default color for logos on this document, it can be overwritten at any time locally
% possible supported values are white, black and blue (any other value from white/black defauklts to blue)
logo .str_set:N = \l_ic_logo_color_str,
logo .initial:n = blue,
logo .value_required:n = true,
% handle remaining options given
unknown .code:n = {
\iow_term:x {
Passing~option~\CurrentOption \c_space_tl to~
{\g_icbook__base_class_str}
}
\PassOptionsToClass { \CurrentOption } { \g_icbook__base_class_str }
}
}


% % All unkown options get passed on to the base class. This allows all options of the base KOMAScript classes to be used.
% \DeclareUnknownKeyHandler {
%     \tl_if_blank:oTF {#2} {
%         \ClassWarning{ic-book}{ Passing~unknown~option~'#1'~to~base~class~'\str_use:N \g_icbook__base_class_str' }
%         \PassOptionsToClass { #1 } { \str_use:N \g_icbook__base_class_str }
%       }
%       {
%         \ClassWarning{ic-book}{ Passing~unknown~key~'#1'~with~value~'#2'~to~base~class~'\str_use:N \g_icbook__base_class_str' }
%         \PassOptionsToClass { #1 = #2 } { \str_use:N \g_icbook__base_class_str }
%       }
%   }

% \ProcessKeysOptions also handles global "class" options. This is the intended way, as we want to react to global changes e.g. language selections
%  see: https://tex.stackexchange.com/a/371754/53868
% process options for this class
% \ProcessKeysOptions{l3icbook}
% process remaining options
% \ProcessKeyOptions

% process options
\@ifundefined{ProcessKeyOptions}
  {%
    \RequirePackage{l3keys2e}%
    \ProcessKeysOptions{l3icbook}
  }
  {\ProcessKeyOptions[l3icbook]} % use LaTeX's new interface if available




%% Pre-Package-Loading:

% scrlayer-scrpage is loaded by base class scrbook. It needs to be configured to correctly display section titles in headers
\PassOptionsToPackage{
    % mandatory, if \leftmark and \rightmark must be used in oneside documents
    autooneside = false,
    % uses chapter AND section title for titlehead
    automark,
    % headsepline=0.25pt, % to show the footwidth
    % TODO: remove again
    footsepline=0.75pt, % to show the footwidth
    plainfootsepline=true, % to show the footwidth
  }{scrlayer-scrpage}










% This option ensures, that the layout typesetting of scrbook, done by typearea, is compatible with the required layout changes for the titlepage implemented through the geometry package.
\PassOptionsToPackage{usegeometry}{typearea}


% Disable the use of file externalization for package 'xsim'. If 'xsim' is not in the features used, this option is ignored.
\PassOptionsToPackage{no-files}{xsim}
\PassOptionsToPackage{blank}{xsim}


% Set the default color model of package xcolor to rgb, as the main use will be to view documents on screens.
% TODO: in the future and if wanted, we shouldimplement a solution for using CMYK, HKS and Pantone colours for print
\PassOptionsToPackage{rgb}{xcolor}


% For package 'enumitem', pass options for inline enumerations and?
\PassOptionsToPackage{shortlabels}{enumitem}
\PassOptionsToPackage{inline}{enumitem}


\PassOptionsToPackage{colorlinks}{hyperref}


%% Load base class
% general development paradigm: load the packages with as few options as possible here. If options needs to be given, define them in the ``%% Pre-Package-Loading`` section using ``\PassOptionsToPackage{<option>}{<package>}''. Preferrably, you should use the package's own setup macro.
\LoadClassWithOptions{\g_icbook__base_class_str}

% in order to supress multiple warnings regarding \float@addtolist in conjunction with \chapter, floatrow et al. load this package as a workaround. KOMAScript author Markus Kohm is working on a package floatrowbytocbasic(2023), but this did no resolve the issue with \chapter for now.
% TODO: possibly replace this in the future.
\RequirePackage{scrhack}


%% Load Packages: Typography

% load the main document font (latin modern is a typographically enhanced version of the default computer modern font and part of any TeX distribution) -> cfr-lm has better support for numbers, but uses same font
%\RequirePackage{lmodern}
\RequirePackage{cfr-lm}

% load fontspec for using custom RWTH font HelveticNeueLTCom
% NOTE: this packages mandates the use of the lualatex engine
\sys_if_engine_luatex:TF
  {
    \RequirePackage{fontspec}
    \setsansfont{HelveticaNeueLTCom}
  }
  {
    \RequirePackage{ClearSans}
    \def\sfdefault{\clearfamily}
  }


% load the typographic enhancements needed for precice optical alignment. No commands from this package are used directly, it is just optimizing kerning.
\RequirePackage{microtype}

% enumitem replaces the default enumerate itemize and description environments by their more capable enumitem counterparts. The default syntax is kept and only extended by additioal optional arguments.
\RequirePackage{enumitem}

%% Load Packages: visuals and page layouting
% requirement for customizing the base KOMA-Script, i.e. scrbook
\RequirePackage{scrlayer-scrpage} % scrpage2 is obsolete

% needspace adds command \needspace to reserve space on the same page, or breaking the whole content to the next page
\RequirePackage{needspace}

% Load the geometry package for resizing margins: this is necessary to create the Schmutztitel (otherwise, this would need to be done in TikZ, which obviously is always an option for me)
\RequirePackage{geometry}

% Package graphicx is required for including external pdf files. It is mandatory for typesetting the chair logo.
\RequirePackage{graphicx}

% Package tikz is required for sensibly typesetting the coverpage and is helpful throughout most documents
\RequirePackage{tikz}

% Package multicol is required for displaying different numbers of  authors  in a suitable fashion. The alternative was a really nasty piece of code, with lots of loops to first dissect and reorder the authors, then merge them together in the right manner. Despite being an extra packge, multicol handles all those simulataneously and provides the multicol environment into the document.
\RequirePackage{multicol}

%% Load Packages: i18n
% babel is a package for setting up languages for the default document titles, e.g. using babel, the title displayed over \tableofcontents will say "Contents" for documents setup in English, while it will say "Inhaltsverzeichnis" for documents typeset in German. Also, labels for figures, tables, etc. are bing translated.
\RequirePackage{babel}
% babel suggest the use of csquotes for internationalized quoting
\RequirePackage{csquotes}

% On some instances, text needs to be translated into the respective language. The translator package provides this option and was the preferred choice, as it also is used inside the beamer package.
% INFO: translator package is not good enough, so I switched to the better translations package
% \RequirePackage{translator}
\RequirePackage{translations}

%% Load Packages: colors
% use package xcolor to define and redefine colours throughout documents. Package is necessary for Schmutztitel and very common for use with a lot of different packages.
\RequirePackage{xcolor}
\input{ic.colors.clo}

%% Load Packages: floating environments
% package used for having sub-figures in figures, i.e. figures belonging together, but having their own name to refer
\RequirePackage{caption}
\RequirePackage{floatrow}
\RequirePackage{subcaption}

% use placeins package to fine-tune placement of floats
\RequirePackage{placeins}

% use package lastpage to reference the last page number
\RequirePackage{lastpage}






%% Load Packages: common mathamatic commands
% since we mainly focus in mathematical subjects, loading of required environments and symbols is needed. amssymb loads math fonts and special symbols, amsmath loads the general equation environments. Some special symbols are not present: to have \coloneqq and \eqqcolon (the definition operator), we need mathtools. To display physical units adequately (mostly in technical docuumentations), we recommend package siunitx to adhere to the SI notation standard. Laplace operators are typeset using the mathrsfs package. To typeset the differential operator, e.g. in integrals, use \dd{\tau} from the physics package, which also defines a consistent way for typesetting partial derivatives correctly.
\RequirePackage{amssymb}
\RequirePackage{amsmath}

% mathtools provides lots of useful extensions to default amsmath symbols, macros and environments.
% For a more detailed explanation, consider the mathtools documentation.
% TODO: cosider using option 'showonlyrefs', but can be used in customization by \mathtoolset{showonlyrefs=true|false}
\RequirePackage{mathtools}

% Package siunitx provides a consistent way to typeset numbers and units independently from a chosen locale.
% This means, that \SI{9.81}{\metre\per\kilogram\second^2} will typeset commata and 1000-separation points with respect to the language chosen for the document either as a comma or as a period.
% use \num[]{23.5} to display numbers in suitable locale formats (automatically switches between . and , depending on selected language, can format large numbers automatically, can give error margins)
% use \si[]{\metre} do display units alone
% use \SI[]{9.81}[]{\newton\per\kilogram} to display numbers WITH units
% for additional options see the comprehensive siunitx documentation
\RequirePackage{siunitx}

% additional font required for suitable Laplace operator symbol
% use \mathscr{L}
\RequirePackage{mathrsfs}

% interval package allows a consistent display of mathematical intervals (open, closed).
% Interval delimiters may be changed at a later point in a customized preamble, to adhere to the document author's taste or publishing requirements.
\RequirePackage{interval}

% typeset differentials and derivative operations in a structured way
\RequirePackage{derivative}


%% Load Packages: Theorems
% TODO: consider removing amsthm and thmtools in favour of more advanced theorem techniques, which don't use \item: the document-author interface should stay the same, though.
% amsthm allows the usage of theorem environments, like theorem or proofs. thmtools allows for easier customization and definition of new environments, such as example, lemma, proposition.
\RequirePackage{amsthm}
% thmtools is a wrapper for amsthm which allows easier creation of theorem-like environments.
\RequirePackage{thmtools}

% TODO: reconsider: in the future, the functionality of xsim might be provided by the same engine providing environments for theorems, definitions and such. Possible candidates involve phfthm, proof-at-the-end.
% xsim is an engine for typesetting pairs of problems and solutions
\RequirePackage{xsim}



%% Load Packages: referencing
\RequirePackage{hyperref}
\RequirePackage{cleveref}
\RequirePackage{bookmark}


%% Load packages for common tasks of drawing diagrams and figures
\ExplSyntaxOff
\RequirePackage{calc}
\RequirePackage{pgfplots}
\usetikzlibrary{backgrounds}
\usetikzlibrary{positioning}

\usetikzlibrary{intersections}
\usetikzlibrary{calc}
\usetikzlibrary{positioning}
\usetikzlibrary{patterns, angles, quotes}
\usetikzlibrary{babel}
\usetikzlibrary{fadings,decorations.pathreplacing}
\usetikzlibrary{arrows}
\usetikzlibrary{arrows.meta}
\usetikzlibrary{backgrounds}
\usetikzlibrary{fit}
\usetikzlibrary{math}
\ExplSyntaxOn



%% Setup document




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Setup fonts

% redefine font sizes to match memoir class (KOMAScript is a bit less consistent)
% NOTE: workaround to reuse the memoir fontsize definitions
\newcommand*{\@ivpt}{4}
\newcommand*{\@xxxpt}{30}
\newcommand*{\@xxxvipt}{36}
\newcommand*{\@xlviiipt}{48}
\newcommand*{\@lxpt}{60}
\newcommand*{\@lxxiipt}{72}
\newcommand*{\@lxxxivpt}{84}
\newcommand*{\@xcvipt}{96}
\newcommand*{\@cviiipt}{108}
\newcommand*{\@cxxpt}{120}
\newcommand*{\@cxxxiipt}{132}
\newlength{\onelineskip}
\newlength{\lxvchars}
\newlength{\xlvchars}
\newif\ifextrafontsizes
\extrafontsizestrue

\dim_case:nnF { \g_icbook__fontsize_dim }
  {
    {9pt} {\input{mem9.clo}}
      {10pt} {\input{mem10.clo}}
      {11pt} {\input{mem11.clo}}
      {12pt} {\input{mem12.clo}}
      {14pt} {\input{mem14.clo}}
      {17pt} {\input{mem17.clo}}
      {20pt} {\input{mem20.clo}}
      {25pt} {\input{mem25.clo}}
      {30pt} {\input{mem30.clo}}
      {36pt} {\input{mem36.clo}}
      {48pt} {\input{mem48.clo}}
      {60pt} {\input{mem60.clo}}
  }{
    % default?
    \input{mem10.clo}
  }

% define font series switches. These must not be used directly in documents, as they are SWITCHES and not commands.
\DeclareRobustCommand{\elseries}{\fontseries{el}\selectfont}
\DeclareRobustCommand{\lseries}{\fontseries{l}\selectfont}
\DeclareRobustCommand{\ltseries}{\fontseries{lt}\selectfont}
\DeclareRobustCommand{\ltxseries}{\fontseries{ltx}\selectfont}
\DeclareRobustCommand{\bxseries}{\fontseries{bx}\selectfont}
\DeclareRobustCommand{\bcseries}{\fontseries{bc}\selectfont}
\DeclareRobustCommand{\mdcseries}{\fontseries{mdc}\selectfont}
\DeclareRobustCommand{\mdxseries}{\fontseries{mdx}\selectfont}

% (Re-)Set fonts for KOMA-Script defined sectioning
\setkomafont{part}{\rmfamily\HUGE\bfseries}
\setkomafont{partnumber}{\rmfamily\Large}
\setkomafont{chapter}{\normalfont\normalcolor\HUGE\scshape}
\setkomafont{section}{\normalfont\normalcolor\LARGE\scshape}
\setkomafont{subsection}{\normalfont\normalcolor\Large\scshape}
\setkomafont{subsubsection}{\normalfont\normalcolor\large\scshape}
\setkomafont{paragraph}{\normalfont\normalcolor\normalsize\scshape}
\setkomafont{subparagraph}{\normalfont\normalcolor\normalsize\scshape}
% \setkomafont{pageheadfoot}{\normalfont\normalcolor\small\scshape}
\setkomafont{title}{\rmfamily\HUGE\bfseries}
\setkomafont{subtitle}{\rmfamily\large}
\setkomafont{subject}{\rmfamily\large}
\setkomafont{author}{\rmfamily\large}
\setkomafont{date}{\rmfamily\large}
\setkomafont{caption}{\normalfont\small}
\setkomafont{descriptionlabel}{\normalfont\normalsize\bfseries}

\setkomafont{pagefoot}{\normalfont\footnotesize\sffamily\lseries}
\addtokomafont{pagefoot}{\color{black50}}
\setkomafont{pagehead}{\normalfont\footnotesize\sffamily\lseries}
\addtokomafont{pagehead}{\color{black50}}

%% Configure footer and header
% footer and header width are taken from the official RWTH styleguide by measuring: left and right margin should be 1cm each
\KOMAoption{footwidth}{\paperwidth-20mm-\ta@bcor}
\KOMAoption{headwidth}{\paperwidth-20mm-\ta@bcor}





%% Modify pagestyles
% modify layer for footer to align at the bottom of the page
\ModifyLayer[
    %  foot,
    hoffset = 1cm,
    align = t,
    voffset=\paperheight-\dim_max:nn {\footheight} {\onelineskip},
  ]{plain.scrheadings.foot.oneside}
\ModifyLayer[
    hoffset = 1cm+\ta@bcor,
    align = t,
    voffset=\paperheight-\dim_max:nn {\footheight} {\onelineskip},
  ]{plain.scrheadings.foot.odd}
\ModifyLayer[
    hoffset = 1cm+\ta@bcor,
    align = t,
    voffset=\paperheight-\dim_max:nn {\footheight} {\onelineskip},
  ]{plain.scrheadings.foot.even}

% modify the layer containing the footsepline to be just above the footer text
\ModifyLayer[
    hoffset = 1cm+\ta@bcor,
    align=b,
    voffset=\paperheight-\dim_max:nn {\footheight} {0.7\onelineskip},
  ]{plain.scrheadings.foot.above.line}

% declare a new layer to hold the logo
\DeclareNewLayer[
    background,
    % head,
    % align=tr,
    % TODO: ensure proper size of area
    area={0pt}{0pt}{\paperwidth}{3cm},
    contents={
        % take up all horizontal space to shift the logo right
        \hfill
        \includegraphics[ height=24mm]{rwth_ic_ \str_use:N \g_language_str   \ic_logocolor_to_str:N \l_ic_logo_color_str}
        % INFO: the additional spacing of 6.2mm has been made to match the width of the footsepline and headsepline.
        \hspace*{6.2mm}
      }
  ]{ic.logohead}

% define custom pagestyles
\DeclarePageStyleByLayers[]{plain.icheadings}{
    plain.scrheadings.head.odd,
    plain.scrheadings.head.even,
    plain.scrheadings.head.oneside,
    plain.scrheadings.head.above.line,
    plain.scrheadings.head.below.line,
    plain.scrheadings.foot.odd,
    plain.scrheadings.foot.even,
    plain.scrheadings.foot.oneside,
    plain.scrheadings.foot.above.line
  }

\DeclarePageStyleByLayers[]{plain.icheadings.logo}{
    ic.logohead,
    plain.scrheadings.head.odd,
    plain.scrheadings.head.even,
    plain.scrheadings.head.oneside,
    plain.scrheadings.head.above.line,
    plain.scrheadings.head.below.line,
    plain.scrheadings.foot.odd,
    plain.scrheadings.foot.even,
    plain.scrheadings.foot.oneside,
    plain.scrheadings.foot.above.line
  }

%% Set default pagestyles
\pagestyle{plain.icheadings}
\renewcommand{\chapterpagestyle}{plain.icheadings}
 \renewcommand{\indexpagestyle}{plain.icheadings}






 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% Configure  TOC

 % make only entries up to level 1 appear in the TOC by default
 \setcounter{tocdepth}{1}

 % TOC entry for chapters
 \DeclareTOCStyleEntry[
     entryformat =\usekomafont{chapter}\normalsize,
     pagenumberformat=\normalfont,
   ]{ tocline }{ chapter }

 % TOC enzry for sections
 \DeclareTOCStyleEntry[
     entryformat =\normalfont\normalsize,
     indent= 0pt,
     dynindent=true,
     indentfollows = chapter,
     linefill=\textcolor{black50}\TOCLineLeaderFill,
   ]{ tocline }{ section }

 % TOC entry for subsections
 \DeclareTOCStyleEntry[
     entryformat =\normalfont\normalsize,
     indent= 0pt,
     dynindent=true,
     indentfollows = chapter,
     linefill=\textcolor{black50}\TOCLineLeaderFill,
   ]{ tocline }{ subsection }

 % TOC entry for subsubsections
 \DeclareTOCStyleEntry[
     entryformat =\normalfont\normalsize,
     indent= 0pt,
     dynindent=true,
     indentfollows = chapter,
     linefill=\textcolor{black50}\TOCLineLeaderFill,
   ]{ tocline }{ subsubsection }



 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% setup enumerations
 % Update the default enumeration environments to use a consistend classic numbering scheme and use less space
 \setlist[itemize,1]{
     label={·},
     labelsep=2.5mm,
     leftmargin=*,
   }
 \setlist[itemize,2]{label={·}, labelsep=2.5mm, leftmargin=*}
 \setlist[enumerate,1]{
     label={\arabic*.},
     font=\footnotesize,
     labelsep=2.5mm,
     leftmargin=*,
     %topsep=2cm,
   }
 \setlist[enumerate,2]{
     label={\roman*.},
     font=\footnotesize,
     labelsep=2.5mm,
     leftmargin=*
   }
 \setlist[description,1]{labelindent=1em}





 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% Setup: visuals and graphics
 % load mandatory tikz libraries
 \ExplSyntaxOff
 \usetikzlibrary{backgrounds}
 \usetikzlibrary{positioning}
 \usetikzlibrary{calc}
 \ExplSyntaxOn




 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% Setup: floating environments
 % Configure the default behaviour for figure captions
 \captionsetup[figure]{
     font=smaller,
   }
 \captionsetup[subfigure]{
     font=smaller,
     singlelinecheck=off,
     justification=raggedright,
   }





 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% Setup: mathamatic symbols
 % configure intervals to use the default notation
 \intervalconfig{
     left ~ open ~ fence = \lparen,
     right ~ open ~ fence = \rparen
   }

 %% configure the derivative package for a common base notation
 \derivset{\pdv}[
     delims-eval=.|,
   ]
 \derivset{\odv}[
     delims-eval=.|,
   ]



 \input{ic.operators.document-layer.clo}


 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% Setup: Layout

 % load the \maketitle implementation
 \input{ic.fcn.maketitle.implementation-layer.clo}
 \input{ic.fcn.maketitle.document-layer.clo}

 % always print sectioning numbers without trailing dot (normally, KOMA decides and handles this in different ways)
 \KOMAoptions{numbers=noenddot}

 % Make scrbook prepend prefixes into appendix sections, e.g. "Appendix A" instead of just "A".
 \KOMAoption{appendixprefix}{true}
 \KOMAoption{headings}{onelineappendix}

 % by default, every sectioning should be numbered. If this is not desired, the author should use the starred sectioning commands or manually override the secnumdepth value according to
 % -1 part
 % 0 chapter
 % 1 section
 % 2 subsection
 % 3 subsubsection
 % 4 paragraph
 % 5 subparagraph
 \setcounter{secnumdepth}{\subparagraphnumdepth}

 % set header and footer marks
 % INFO: override this, if you need a custom footer/header text. See scrguide.pdf for precise instructions of the meaning
 % INFO: use macro \verbar o separate multiple parts of the footer/header
 \ExplSyntaxOff
 \ohead*{}
 \chead*{}
 \ihead*{}
 % \ifoot*{}
 \cfoot*{\thepage~\GetTranslation{of}~\pageref{LastPage}}
 % \ofoot*{}
 \ExplSyntaxOn


 % in order to place the footer exactly, hook into typearea recalculation.
 \AfterCalculatingTypearea{
     \setlength{\footskip}{1.5in}
     \setlength{\textheight}{\paperheight-\topmargin-1in-\voffset+\topmargin-\headheight-\headsep-\footskip}

   }
 \KOMAoptions{DIV=last}

 %% define a vertical bar to separate header and footer parts
 \DeclareDocumentCommand{\vertbar}{}{
     \kern1pt\rule[-\dp\strutbox]{.6pt}{\baselineskip}\kern1pt
   }

 %% move section numbers into margins, so section titles align with the section body texts: makes new sections much more recognizable and easier to find by just skipping pages
 % https://tex.stackexchange.com/a/241916/53868
 \DeclareDocumentCommand \chapterlinesformat { m m m } {
     \makebox[0pt][r]{
         \usekomafont{chapter} #2
           {\normalsize\hspace{1em}}
       }
     #3
   }

 \DeclareDocumentCommand \sectionlinesformat { m m m m } {
      \makebox[0pt][r] {%
          \str_if_eq:nnT { #1 } { section } {
              %{\usekomafont{section}\fontsize{10.85}{18}\selectfont#3}
              %\usekomafont{subsection} \textto{\textcolor{black75} #3}
              \usekomafont{subsection} {\textcolor{black75} #3}
                {\normalsize\hspace{1em}}
            }
          \str_if_eq:nnT { #1 } { subsection } {%
              %{\usekomafont{subsection}\fontsize{7.55}{\@xiipt}\selectfont#3}
              %{\usekomafont{subsection}\textto{\textcolor{black75}#3}}%
              %\usekomafont{subsection} \textto{\textcolor{black50} #3}
              \usekomafont{subsection} {\textcolor{black50} #3}
                {\normalsize\hspace{1em}}
            }%
          \str_if_eq:nnT { #1 } { subsubsection } {%
              %{\usekomafont{subsection}\fontsize{7.55}{\@xiipt}\selectfont#3}
              %{\usekomafont{subsection}\textto{\textcolor{black75}#3}}%
              %\usekomafont{subsubsection} \textto{\textcolor{black50} #3}
              \usekomafont{subsubsection} {\textcolor{black50} #3}
                {\normalsize\hspace{1em}}
            }%
        }%
      #4%
    }
  \DeclareDocumentCommand \sectioncatchphraseformat  { m m m m } {
      \makebox[0pt][r] {%
          \str_if_eq:nnT { #1 } { paragraph } {%
              %{\usekomafont{subsection}\fontsize{7.55}{\@xiipt}\selectfont#3}
              %{\usekomafont{subsection}\textto{\textcolor{black75}#3}}%
              %\usekomafont{paragraph} \textto{\textcolor{black50} #3}
              \usekomafont{paragraph} {\textcolor{black50} #3}
                {\normalsize\hspace{1em}}
            }%
          \str_if_eq:nnT { #1 } { subparagraph } {%
              %{\usekomafont{subsection}\fontsize{7.55}{\@xiipt}\selectfont#3}
              %{\usekomafont{subsection}\textto{\textcolor{black75}#3}}%
              %\usekomafont{subparagraph} \textto{\textcolor{black50} #3}
              \usekomafont{subparagraph} {\textcolor{black50} #3}
                {\normalsize\hspace{1em}}
            }%
        }%
      #4%
    }


  % remove trailing paragraph ends from chapter, section and subsection numbers. These are manually added inside \chapterlineformat and \sectionlinesformat
  \DeclareDocumentCommand {\chapterformat}  { } { \thechapter }
 \DeclareDocumentCommand {\sectionformat} { } {\thesection}
  \DeclareDocumentCommand \subsectionformat { } {\thesubsection}
    \DeclareDocumentCommand \subsubsectionformat {} {\thesubsubsection}
    \DeclareDocumentCommand \paragraphformat {} {\theparagraph}
    \DeclareDocumentCommand \subparagraphformat {} {\thesubparagraph}


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Setup: theorem environments

    % declare reusable theorem styles
    \declaretheoremstyle[
        headpunct={},
        notebraces={\lparen}{\rparen},
        headindent=0pt,
        postheadspace=1em,
        %   spaceabove=1em,
        %   spacebelow=2em,
        bodyfont=\normalfont,
        headfont=\normalfont\bfseries,
        notefont=\normalfont,
        qed=\thmendsymbol,
      ]{theoremstyle}
    \declaretheoremstyle[
        headpunct={},
        notebraces={\lparen}{\rparen},
        headindent=0pt,
        postheadspace=1em,
        spaceabove=1em,
        spacebelow=1em,
        bodyfont=\normalfont,
        headfont=\normalfont\scshape\bfseries,
        notefont=\normalfont,
        qed=\qedsymbol,
      ]{proofstyle}

    % empty command to be overridden if hyperref and bookmark have been loaded
    \DeclareDocumentCommand {\@theorembookmark} {} {%
      }

    % load additional theorem-like environments
    \input{ic.env.definition.document-layer.clo}
    \input{ic.env.lemma.document-layer.clo}
    \input{ic.env.theorem.document-layer.clo}
    \input{ic.env.proposition.document-layer.clo}
    \input{ic.env.corollary.document-layer.clo}
    \input{ic.env.example.document-layer.clo}
    \input{ic.env.remark.document-layer.clo}
    \input{ic.env.problem.document-layer.clo}
    \input{ic.env.solution.document-layer.clo}


    % setup default numbering for equations inside theorem-like environments:
    % use a new separate counter
    \newcounter{thmsubequation}
    % without the optional first argument, this refers to all theorem-like environments. See documentation of thmtools.
    \addtotheorempreheadhook{
        % save original values for overrides:
        \let\c@equation@original = \c@equation
        \let\theequation@original = \theequation

        % reset the counter to start
        \setcounter{thmsubequation}{0}
        % temporarily make refstepcounter increment the custom counter
        \let\c@equation\c@thmsubequation
        % replace the equation numbering output to use the custom format and counter
        \renewcommand{\theequation}{\csname the\thmt@envname \endcsname.\arabic{thmsubequation}}
      }

    % 'problem' template without visible additions
    \DeclareExerciseEnvironmentTemplate{problem-template}{
        \IfExercisePropertySetTF{title}{%
            \thmproblem[{\GetExerciseProperty{title}}]
          }{
            \thmproblem
          }
        % \samepage\mbox{}\samepage
      }{
        \endthmproblem
      }

    % 'solution' template, appears without colored additions
    \DeclareExerciseProperty{title}
    \DeclareExerciseEnvironmentTemplate{solution-template}{
        \IfExercisePropertySetTF{title}{%
            \thmsolution[\GetExerciseProperty{title}]
          }{
            \thmsolution
          }
      }{
        \endthmsolution
      }

    % define a problem/solution xsim exercise pair
    \DeclareExerciseType{problem}{
        exercise-env    = problem ,
        solution-env    = solution ,
        exercise-name   =,% \GetTranslation{problem},
        exercises-name   =,% \ranslate{problems},
        solution-name   =,% \GetTranslation{solution},
        solutions-name   =,% \GetTranslation{solutions},
        exercise-template = problem-template,
        solution-template = solution-template,
      }

    \input{ic.fcn.covercontent.implementation-layer.clo}
    \input{ic.fcn.covertitle.implementation-layer.clo}

    \input{ic.fcn.covercontent.document-layer.clo}
    \input{ic.fcn.covertitle.document-layer.clo}
    \input{ic.fcn.thmendsymbol.document-layer.clo}


    \str_case_e:nnF { \languagename }
      {
        {german} {\str_gset:Nn \g_language_str {de}}
          {ngerman} {\str_gset:Nn \g_language_str {de}}
      }
      {
        % default language
        \str_gset:Nn \g_language_str {en}
      }

    % process locale and color information for the correct logo choice
    \cs_new_nopar:Nn \ic_logocolor_to_str:N {
        \str_case:VnF #1
          {
            {white}{
                % \str_set:Nx \l_ic_logo_str {rwth_ic_  \g_language_str  _rgb_w}
                _rgb_w
              }
              {black}{
                _rgb_b
              }
          }
          {
            % otherwise use the default blue version
            _rgb
          }
        % \str_use:N \l_ic_logo_color_str
      }


    \ExplSyntaxOff
    \DeclareTranslation{English}{template_chair_str}{
        Chair~of~Intelligent~Control~Systems\\
        ICT~cubes,~floor~3\\
        Kopernikusstraße~16\\
        D-52074~Aachen,~Germany
      }
    \DeclareTranslation{German}{template_chair_str}{
        Lehrstuhl~für~Intelligente~Regelungssysteme\\
        ICT~cubes,~3.~Etage\\
        Kopernikusstraße~16\\
        52074~Aachen
      }
    \DeclareTranslation{English}{of}{of}
    \DeclareTranslation{German}{of}{von}
    \ExplSyntaxOn


    % Set the primary affiliations for this document (default is the chair)
    % this command is intended to be used in the document environment before calling \maketitle
    %  set the default primary affiliation
    % NOTE: you may override this at the start of your document by setting your own content in \lowertitleback (or add \uppertitleback)
    \lowertitleback{%
        \centering%
        \begin{tabular}{r l }%
          %   \@date \\%
          %   \hspace{-2.5mm}
          \includegraphics[height=16mm]{rwth_ic_ \str_use:N \g_language_str _rgb_b} &
          % \@chair%
          \begin{minipage}[b]{0.618\linewidth}
            \small
            \sffamily
            \lseries
            \color{black75}
            \GetTranslation{template_chair_str}
          \end{minipage}
        \end{tabular}%
      }%


    \endinput
