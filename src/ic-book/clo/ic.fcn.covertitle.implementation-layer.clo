% define the custom maketitle typesetting
\cs_set:Nn \covertitle: {
    % handle logo color with switch command:
    
    
    
    \str_case:VnF \l_ic_covertitle__logo
      {
        {white}{
            \str_set:Nx \l_logoname_str {rwth_ic_  \g_language_str  _rgb_w}
          }
          {black}{
            \str_set:Nx \l_logoname_str {rwth_ic_  \g_language_str  _rgb_b}
          }
      }
      {
        % otherwise use the default blue version
        \str_set:Nx \l_logoname_str {rwth_ic_  \g_language_str  _rgb}
      }
    %
    %
    % output the Schmutztitel page
    \begin{titlepage}
      %
      % make the default color in this titlepage environment WHITE
      \color{\l_ic_covertitle__color}
      %
      % disable parindents for the covertitle
      \dim_set:Nn \parindent {0pt}
      
      \thispagestyle{empty}
      
      
      
      
      
      \coffin_new:N \l_covertitle__titlestuff_coffin
      \coffin_new:N \l_covertitle__subject_coffin
      \coffin_new:N \l_covertitle__title_coffin
      \coffin_new:N \l_covertitle__subtitle_coffin
      \coffin_new:N \l_covertitle__covercontent_coffin
      \coffin_new:N \l_covertitle__authors_coffin
      
      
      \vcoffin_set:Nnn  \l_covertitle__title_coffin {\paperwidth-2in} {
          \begin{minipage}[t]{\linewidth}
            \sffamily
            \elseries
            \Large
            \@subject
            \par
          \end{minipage}
          \vspace*{3em}\\
          \begin{minipage}[t]{\linewidth}
            \sffamily
            \lseries
            \spaceskip=2\fontdimen2\font plus 3\fontdimen3\font minus 3\fontdimen4\font
            \HUGE
            \@title
            \par
          \end{minipage}
          \vspace*{2em}\\
          \begin{minipage}[t]{\linewidth}
            \sffamily
            \elseries
            \spaceskip=2\fontdimen2\font plus 3\fontdimen3\font minus \fontdimen4\font
            \LARGE
            \@subtitle
            \par
          \end{minipage}
        }
      
      \vcoffin_set:Nnn  \l_covertitle__covercontent_coffin {\paperwidth} {%
          \tl_use:N \g_covercontent_tl
        }
      
      \coffin_join:NnnNnnnn \l_covertitle__titlestuff_coffin {t} {l}\l_covertitle__title_coffin {l} {t} {0pt} {0pt}
      
      \vcoffin_set:Nnn  \l_covertitle__authors_coffin {\paperwidth-2in-\hoffset} {
          \group_begin:
          \seq_new:N \l_authors_seq
          \cs_generate_variant:Nn \regex_split:nnNTF { noNTF }
          % split the contents of \@author (from scrbook) by \and commands and place matches inside \l_authors_seq, then handle the matched code (T) or unmatched code (F).
          \regex_split:noNTF{ [\c{and}|,] } { \@author } \l_authors_seq
            {
              % multiple authors found
              \seq_clear_new:N \l_authors_boxes_seq
              \seq_set_map:NNn  \l_authors_boxes_seq \l_authors_seq {
                  \begin{minipage}{0.333333\linewidth}
                    \ignorespaces
                    \sffamily
                    \elseries
                    \Large
                    ##1
                    \hfill
                  \end{minipage}
                  % #1
                }
              \seq_use:Nn \l_authors_boxes_seq
                {
                  %     % no separator
                }
            }
            {
              % No match found, i.e. one single author
              \sffamily
              \elseries
              \Large
              \@author
            }
          \group_end:
        }
      
      
      
      \begin{tikzpicture}[
        remember~picture,
        overlay,
        background ~ rectangle / .style = {
        fill = \l_ic_covertitle__background,
        },
        every ~ node/.append ~ style = {
        inner ~ sep = 0,
        },
        show~background~rectangle,
        ]
        \node[
            inner~sep = 0,
            anchor = north~east,
          ] at ($(current~page.north~east) + (-6.2mm , 0mm)$) {
            % NOTE about the coordinate shift: The logo files contain the "Schutzraum" in all directions, which is 2x "R-bar" width horizontally. At the selected logo height of 24mm, this comes down to a right Schutzraum margin of 2x 1.90mm = 3,8mm. To achieve the matching margin of 10mm with header and footer, the node is being shifted by 3,8mm-10mm = 6,2mm.
            % TODO: make logo size depend on paper size
            \includegraphics[ height=24mm]{\l_logoname_str}
            % INFO: the logo file already contains the Schutzraum around it
          };
        \node[
            anchor = north~west,
            below~right = 0.2\paperheight ~ and ~ 1in+\hoffset ~ of ~ current ~ page.north~west,
          ] (titlestuff) {
            \coffin_typeset:Nnnnn \l_covertitle__titlestuff_coffin {t} {l} {0pt} {0pt}
          };
        \node[
            anchor = north~west,
            below ~right = 0.5\paperheight ~ and ~ 0pt ~ of ~ current~page.north~west,
            minimum ~ height = 0.3\paperheight,
            minimum ~ width = \paperwidth,
          ] (freeformtitle) {
            \coffin_typeset:Nnnnn \l_covertitle__covercontent_coffin {t} {l} {0pt} {0pt}
          };
        \node[
            anchor = south~west,
            above~right = 0.1\paperheight ~ and ~ 1in+\hoffset ~ of ~ current ~ page.south~west,
          ] (authors) {
            \coffin_typeset:Nnnnn \l_covertitle__authors_coffin {t} {l} {0pt} {0pt}
          };
      \end{tikzpicture}
      %
      
      %
      
      
      %\coffin_display_handles:Nn \l_covertitle__page_coffin {red}
      
      %\hfill
      % this is the RWTH-styleguide-recommended height for documents in A4
      % TODO: make logo size depend on paper size
      %\includegraphics[ height=16.2mm]{\l_logoname_str}%
      % in order to adhere to the horizontal Schutzraum around the logo, there needs to be one additional horizontal unit of space between the logo and the edge of the page
      % for a calculation/table of the units refer to the official RWTH styleguide
      % 2*(rasterheight) - 2*(rasterwidth)
      %\hspace*{3mm}%
      %\\
      %\allowbreak
      
      % Title box
      % also contains subject
      % indent it manually, as we removed all page margins before
      %\hspace*{1in}
      
      % prepare space for five lines of title + 1 line of subject
      
      % insert the right side margin manually
      %\hspace*{1in}
      
      % force some vertical spacing between the boxes
      %\vspace*{8mm}\\
      
      % Subtitle box
      %\hspace*{1in}
      
      
      
      %\hspace*{1in}
      
      % force some vertical spacing between the boxes
      %\vspace*{2mm}\\
      
      %  frontmatter box
      
      % syntax comment: \begin{minipage}[alignment in minipage {t,c,b}][height of minipage][anchor of minipage{t,c,b}]{width of minipage}
      %\begin{minipage}[t][12cm][t]{\linewidth}
      %	{
      %		\fontsize{15pt}{18pt}
      %		% \selectfont
      %		% \HelveticaNeueLTComThEx\spaceskip=2\fontdimen2\font plus 3\fontdimen3\font minus \fontdimen4\font
      %		% \tl_use:N \g_template_titlepage__freeformtitle_tl
      %	}
      %\end{minipage}
      
      
      % force some vertical spacing between the boxes
      %\vspace*{2mm}\\
      
      %\vfill
      
      % Authors box
      
      %\begin{minipage}[t]{\dimexpr(\linewidth-1in*2)}
      %	{
      %		\fontsize{13pt}{15.6pt}
      %		\selectfont
      %		% \HelveticaNeueLTComLt
      %		\HelveticaNeueLT
      %		\noindent
      %		\lineskip 0.75em
      %		% Split the expanded list, i.e. `\and` will place `,`
      %		\authors_coverpage:
      %	}
      %\end{minipage}
      
      %\hspace*{1in}
      %\vspace*{1in}\\
    \end{titlepage}
    % reset the page counter, such that the Schmutztitel ist not included into the numbering
    \setcounter{page}{0}
  }