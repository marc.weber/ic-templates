require '../../auxiliary_functions'

base = "ic-slides"
module = "ic-slides-template"


maindir = "../.."
builddir = maindir.."/build/"..module

-- make the base class and files available for compilation: do not copy it into this package, just link to the required tds
texmfdir = maindir.."/build/"..base.."/distrib/ctan"

sourcefiledir = maindir.."/src/"..module
sourcefiles = {
    "**/*.cls",
    "**/*.sty",
    "**/*.tex",
    "**/*.tikz",
    "**/*.png",
    "**/*.bib",
    "**/*.pdf"
}

typesetfiles = {
    "demo0.tex"
}
--  NOTE: textfiles only end uo in builddir/ctan, NOT in builddir/tds
textfiledir = maindir
textfiles = {}
-- installfiles may be empty here, as all installation is handled by tdslocations
installfiles = {}


-- tell l3build to arrange the accompanying logo images into the right place in tds
-- for the templates, the tds is slightly abused
tdslocations = {
    "*.tex",
    "*.sty",
    "*.bib",
    "*.tikz",
    "*.png",
    "*.pdf",
}

--  keep hierarchical structure in tds
flattentds = false

-- pack also a copy of tds structure
packtdszip = false