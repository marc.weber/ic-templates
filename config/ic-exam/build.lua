-- define auxiliary functions
local function os_message(text)
  local mymax = 77 - string.len(text) - string.len("done")
  local msg = text.." "..string.rep(".",mymax).." done"
  return print(msg)
end

-- define build targets

if options["target"] == "release-ic" then
    os_message("** test ic-exam")



    -- exit without error
    os.exit(0)
end