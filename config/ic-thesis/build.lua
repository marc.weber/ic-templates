require '../../auxiliary_functions'

module = "ic-thesis"
maindir = "../.."
builddir = maindir.."/build/"..module

sourcefiledir = maindir
sourcefiles = {
    "src/ic-book/clo/*.clo",
    "src/ic-book/*.cls",
    "src/ic-book/*.sty",
    -- the order is very important here: later files overwrite the earlier ones, so thesis-specific files need to overwrite book-specific ones.
    "src/ic-thesis/clo/*.clo",
    "src/ic-thesis/*.cls",
    "src/ic-thesis/*.sty",
    "src/*.clo",
    "fonts/**/*.ttf",
    "src/**/*.fontspec",
    "images/**/*.pdf",
}
--  NOTE: textfiles only end uo in builddir/ctan, NOT in builddir/tds
textfiledir = maindir
textfiles = {}
installfiles = {
    "**/*.cls",
    "**/*.clo",
    "**/*.sty",
    "**/*.ttf",
    "**/*.fontspec"
}

-- tell l3build to arrange the accompanying logo images into the right place in tds
tdslocations = {
    "tex/latex/rwth_ic_*.pdf",
    "fonts/truetype/local/HelveticaNeueLTCom/HelveticaNeueLTCom*.ttf",
    "tex/latex/*.fontspec",
}

--  keep hierarchical structure in tds
flattentds = false

-- pack also a copy of tds structure
packtdszip = false