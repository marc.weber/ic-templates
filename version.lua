bundle = "ic"

pkgversion = "0.9.19"
pkgdate = "2024-08-24"

affiliation = "This file is part of the ic-templates bundle developed for the Chair of Intelligent Control Systems."
repourl = "https://git-ce.rwth-aachen.de/marc.weber/ic-templates"